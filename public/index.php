<?php
session_start();
require '../vendor/autoload.php';
$controller = require_once '../app/Controller.php';

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/', 'getIndex');
    $r->addRoute('GET', '/login', 'getAdmin');
    $r->addRoute('POST', '/api/login', 'postLogin');
    $r->addRoute('DELETE', '/api/logout', 'postLogout');
    $r->addRoute('GET', '/admin', 'getAdmin');
    $r->addRoute('GET', '/admin/{page}', 'getAdmin');
    $r->addRoute('GET', '/admin/{page}/{sub_page}', 'getAdmin');
    $r->addRoute('GET', '/admin/{page}/{sub_page}/{sub_sub_page}', 'getAdmin');
    $r->addRoute('GET', '/api/customize', 'getCustomize');
    $r->addRoute('PUT', '/api/customize', 'putCustomize');
    $r->addRoute('POST', '/api/customize/upload', 'postUploadImage');
    $r->addRoute('GET', '/api/customize/render', 'getCustomizeStyle');
    $r->addRoute('GET', '/api/language', 'getLanguage');
    $r->addRoute('GET', '/api/pages', 'getPages');
    $r->addRoute('PUT', '/api/pages', 'putPages');
    $r->addRoute('PUT', '/api/page/{page}/section', 'putPageSection');
    $r->addRoute('POST', '/api/language', 'postLanguage');
    $r->addRoute('PUT', '/api/language', 'putLanguage');
    $r->addRoute('GET', '/api/language/files', 'getAllLanguageFile');
    $r->addRoute('GET', '/api/social', 'getSocial');
    $r->addRoute('PUT', '/api/social', 'putSocial');
    $r->addRoute('GET', '/{page}', 'getIndex');
});

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri        = rawurldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

//$uri = str_replace($controller->app->config->get('base_path'), '', $uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        header("HTTP/1.0 404 Not Found");
        throw new Exception("Not Found", 1);
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        header("HTTP/1.0 405 Method Not Allowed");
        throw new Exception("Method Not Allowed", 1);
        break;
    case FastRoute\Dispatcher::FOUND:
        if (strpos($uri, 'api') > -1 && !strpos($uri, 'login') && !isset($_SESSION['auth']) && stripos($uri, 'pages') < 0) {
            echo 'Pls login :D';die();
        }
        $handler = $routeInfo[1];
        $vars    = $routeInfo[2];
        call_user_func_array([$controller, $handler], [$vars]);
        break;
}
