<!DOCTYPE html>
<html lang="en" ng-app="app" ng-controller="AppCtrl">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $this->header(); ?>
        <!--<link rel="Shortcut Icon" href="http://www.the-bancroft.com/favicon.png" type="image/x-icon" />-->
        <link rel="stylesheet" href="<?php echo $app->getTemplateDirectoryUri(); ?>/assets/css/theme.min.css">
        <link ng-if="restaurant.getRestaurantID() === 'SG_SG_R_EightCafeBar'" rel="stylesheet" href="<?php echo $app->getTemplateDirectoryUri(); ?>/assets/css/extra/eight/extra.css">
        <?php echo $customize->render(); ?>
        <script src="<?php echo $app->getTemplateDirectoryUri(); ?>/assets/js/app.min.js" defer></script>
    </head>

    <body>
        <div id="wrapper">
            <div class="header">
                <nav class="navbar navbar-fixed-top navbar-static-top">
                    <div class="container-fluid" home-menu>
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header navbar-left-logo">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" href="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a ng-if="restaurant.getRestaurantID() !== 'TH_BK_R_Talayzaa'" class="navbar-brand" ng-click="scrollTop()">
                                <span ng-bind="restaurant.getTitle()"></span>
                            </a>
                            <a ng-if="restaurant.getRestaurantID() === 'TH_BK_R_Talayzaa'" class="navbar-brand" ng-click="scrollTop()" style="padding:0px;">
                                <div style="max-width: 100%;">
                                    <img style="margin-left:20px;max-height: 50px" ng-src="https://website-external.s3.amazonaws.com/media/talaazay/talayzaa-logo-white.png" alt="{{ restaurant.getTitle()}}">
                                </div>
                            </a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav menu_top_navbar" >
                                <li ng-show="pages.homepage.isEnableSection('about')">
                                    <a ng-click="jumpTo('#about-section')" ng-show="pages.homepage.isEnableSection('about')" translate="page_menu.about"></a>
                                </li>
                                <li ng-show="pages.homepage.isEnableSection('menu')">
                                    <a ng-click="jumpTo('#menus')" ng-show="pages.homepage.isEnableSection('menu')" translate="page_menu.menu"></a>
                                </li>
                                <li ng-show="pages.homepage.isEnableSection('event')">
                                    <a ng-click="jumpTo('#event-section')" ng-show="pages.homepage.isEnableSection('event')" translate="page_menu.event"></a>
                                </li>
                                <li ng-show="pages.homepage.isEnableSection('chef')" ng-if="restaurant.getRestaurantID() !== 'TH_BK_R_Talayzaa'">
                                    <a ng-click="jumpTo('#chef-section')" ng-show="pages.homepage.isEnableSection('chef')" translate="page_menu.chef"></a>
                                </li>
                                <li ng-show="pages.homepage.isEnableSection('kitchen')">
                                    <a ng-click="jumpTo('#kitchen-section')" ng-show="pages.homepage.isEnableSection('kitchen')" translate="page_menu.kitchen"></a>
                                </li>
                                <li ng-show="pages.homepage.isEnableSection('gallery')" ng-if="restaurant.getRestaurantID() !== 'SG_SG_R_EightCafeBar'">
                                    <a ng-click="jumpTo('#gallery-section')" ng-show="pages.homepage.isEnableSection('gallery')" translate="page_menu.gallery"></a>
                                </li>
                                <li ng-show="pages.homepage.isEnableSection('contact')">
                                    <a ng-click="jumpTo('#contact-section')" ng-show="pages.homepage.isEnableSection('contact')" translate="page_menu.contact"></a>
                                </li>
                                <li ng-show="pages.homepage.isEnableSection('social')">
                                    <a ng-click="jumpTo('#social-section')" ng-show="pages.homepage.isEnableSection('social')" translate="page_menu.social"></a>
                                </li>
                            </ul>
                            
                            
                            
                            <ul class="nav navbar-nav navbar-right reservation" id="">
                                <li data-toggle="collapse" ng-click="jumpToBookForm()">
                                    <a class="navbar-call-to-action" ng-bind="restaurant.book_button.text"></a>
                                </li>
                                <li>
                                    <a ng-href="{{ fb_page_url}}"><img ng-src="{{asset('images/header_icon1.png')}}" alt=""></a>
                                </li>
                                <li>
                                    <a ng-href="{{ instagram_url}}"><img ng-src="{{asset('images/header_icon00.png')}}" alt=""></a>
                                </li>
                            </ul>
                        </div>
                        <a class="bc-show-list-menu" href=""></a>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </div>
            <div class="content">
                <div class="content-area" ng-show="pages.homepage.isEnableSection('header')">
                    <article class="main">
                        <div class="restaurant-logo">
                            <img ng-if="restaurant.getRestaurantID() !== 'SG_SG_R_EightCafeBar' && restaurant.getRestaurantID() !== 'TH_BK_R_Talayzaa'" ng-src="{{ restaurant.getLogoImage()}}" alt="{{ restaurant.getTitle()}}">
                            <img ng-if="restaurant.getRestaurantID() === 'TH_BK_R_Talayzaa'" ng-src="https://website-external.s3.amazonaws.com/media/talaazay/talayzaa_logo-300px.png" alt="{{ restaurant.getTitle()}}">
                            <img ng-if="restaurant.getRestaurantID() === 'SG_SG_R_EightCafeBar'" ng-src="https://website-external.s3.amazonaws.com/media/eight/eightcafe-white.png" alt="{{ restaurant.getTitle()}}">
                        </div>
                        <div class="entry-content">
                            <h3><!-- Established 2014 --></h3>
                        </div>
                        <h1 class="restaurant-name" ng-bind="restaurant.getTitle()"></h1>
                        <address>
                            <p ng-bind="restaurant.getAddress()"></p>
                            <p><span ng-bind="restaurant.getZip()"></span><span ng-if="restaurant.getZip() != ''">, </span><span ng-bind="restaurant.getCountry()"></span></p>
                            <br/>
                            <p><span ng-bind="restaurant.getPhone()"></span></p>
                        </address>
                        

                        <p ng-click="jumpToBookForm()">
                            <a class="reservations" ng-bind="restaurant.book_button.text"></a>
                        </p>
                    </article>
                </div>
            </div>
            <!-- content torch add slider -->
            <div id="slider" ng-show="pages.homepage.isEnableSection('top_slider')">
                <div id="home-slider" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item" ng-repeat="image in restaurant.getGallery()" ng-class="{'active':$first}">
                            <img ng-src="{{asset('images/slider-top.png')}}" alt="{{ restaurant.getTitle()}}" style="width: 100%; background-image:url('{{ image.getFullsize()}}'); background-position: 50% 50%; background-size: cover">
                            <div class="carousel-caption">
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" ng-click="SliderControl('prev')" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" ng-click="SliderControl('next')" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!-- end content torch add slider -->
            <div class="widget_black_studio_tinymce" id="about-section" ng-show="pages.homepage.isEnableSection('about')">
                <h2 class="widget-title" ng-bind="restaurant.getTitle()"></h2>
                <div style="text-align: center" class="textwidget" ng-repeat="description in restaurant.getDescription()">
                    <h3 ng-bind="description.title"></h3>
                    <p ng-repeat="descBody in description.body" ng-bind="descBody"></p>
                </div>
            </div>
            <div id="menus" ng-show="pages.homepage.isEnableSection('menu')">


                <div class="header-menus">
                    <h3 class="widget-title" translate="page_menu.menu"></h3>
                    <ul class="bc_menus" ng-if="restaurant.getRestaurantID() !== 'TH_BK_R_Talayzaa'">
                        <li ng-repeat="menu in restaurant.getMenus()">
                            <a ng-click="jumpToMenu(menu.categorie)" ng-bind="menu.categorie.value"></a>
                        </li>
                    </ul>
                </div>

                <div class="content">
                    <div class="menu_content">
                        <div class="row menu_content_row_list">
                            <div ng-if="restaurant.getRestaurantID() !== 'TH_BK_R_Talayzaa'">
                                <div class="col-md-6 col-hold-left">
                                    <div class="col-hold" ng-repeat="menu in restaurant.getMenus()" ng-if="$even" id="menu-{{ menu.categorie.ID}}">
                                        <div class="menu-item">
                                            <h3 ng-bind="menu.categorie.value"></h3>
                                            <p ng-repeat="item in menu.items">
                                                <strong ng-bind="item.item_title"></strong>
                                                <br/> <span ng-bind="item.item_description"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-hold" ng-repeat="menu in restaurant.getMenus()" ng-if="$odd" id="menu-{{ menu.categorie.ID}}">
                                        <div class="menu-item">
                                            <h3 ng-bind="menu.categorie.value"></h3>
                                            <p ng-repeat="item in menu.items">
                                                <strong ng-bind="item.item_title"></strong>
                                                <br/> <span ng-bind="item.item_description"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            <div ng-if="restaurant.getRestaurantID() === 'TH_BK_R_Talayzaa'">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="panel-menu panel-menu-default col-hold" style="margin-bottom: 30px;" ng-repeat="menu in restaurant.menu">
                                            <a  data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$index}}"  data-target="#collapse-{{$index}}"  >
                                                <div class="panel-menu-heading " style="border-bottom: 1px solid #ccc;" >
                                                <h3 ng-bind="menu.categorie.value" style="display: inline"></h3>
                                                <i class="fa fa-angle-down" style="display: inline;float: right;"></i>
                                                </div>
                                            </a>
                                            <div id="collapse-{{$index}}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul>
                                                        <li ng-repeat="item in menu.items">
                                                            <p >
                                                                <strong ng-bind="item.item_title"></strong>
                                                                <br/> <span ng-bind="item.item_description"></span>
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>

                
                <div class="header-menus" id="book-now-div">
                    
                    <div  data-toggle="collapse" data-parent="#accordion" href="#collapse-ss" aria-expanded="false" aria-controls="collapse1" class="collapsed">
                        <h3 class="widget-title" >BOOK NOW</h3>
                    </div>

                    <div id="collapse-ss" class="panel-collapse collapse">
                        <div style="width: 100%;text-align:center">
                            <book-iframe restaurant="restaurant"></book-iframe>
                            <!--<iframe ng-src="{{booking_form_url}}" width="600px" height="1150px" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>-->
                        </div>
                    </div>
                </div>
                <div class="private-events" id="event-section" ng-show="pages.homepage.isEnableSection('event')">
                    <h3 class="widget-title" translate="homepage.event.title"></h3>
                    <div class="textwidget">
                        <p translate="homepage.event.description"></p>
                    </div>
                    <div class="events container">
                        <div ng-repeat="event in events" class="col-xs-12 col-sm-4 col-lg-3" style="height: 100%;vertical-align: top;">
                            <div class="event-image">
                                <img ng-src="{{asset('images/slider-bottom.png')}}" alt="{{ restaurant.getTitle()}}" style="width: 100%; background-image:url('{{event.getPicture()}}'); background-position: 50% 50%; background-size: cover">
                            </div>
                            <div class="cnt">
                                <p class="event-title" ng-bind="event.getName()"></p>
                                <span ng-if="restaurant.getRestaurantID() !== 'TH_BK_R_Talayzaa'" class="event-time">{{event.getStartTime('MMM D, YYYY')}} to {{event.getEndTime('MMM D, YYYY')}} in {{event.getCity()}}</span>
                                <p class="event-description" ng-bind="event.getDescription()"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-bottom" id="gallery-section" ng-show="pages.homepage.isEnableSection('bottom_slider')">
                <div id="home-slider" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item" ng-repeat="image in restaurant.getGallery()" ng-class="{'active':$first}">
                            <img ng-src="{{asset('images/slider-bottom.png')}}" alt="{{ restaurant.getTitle()}}" style="width: 100%; background-image:url('{{ image.getFullsize()}}'); background-position: 50% 50%; background-size: cover">
                            <div class="carousel-caption">
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" ng-click="SliderControl('prev')" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" ng-click="SliderControl('next')" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="slider2">
                <div class="slide-container" id="chef-section" ng-show="pages.homepage.isEnableSection('chef')">
                    <h3 translate="homepage.chef.meet_the_chef"></h3>
                    <div class="container">
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <div class="slide-stage">
                                <div class="slide-image">
                                    <a href="http://w3ateam.com"><img ng-src="{{ restaurant.chef.getChefImage()}}" /></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-9">
                            <div class="slider2_entry" ng-if="restaurant.chef.getChefBelieve() !== ''">
                                <h4>
                                    <strong ng-if="restaurant.chef.isFemale()" translate="homepage.chef.her_believe"></strong>
                                    <strong ng-if="!restaurant.chef.isFemale()" translate="homepage.chef.his_believe"></strong>
                                </h4>
                                <p ng-bind="restaurant.chef.getChefBelieve()"></p>
                            </div>
                            <div class="slider2_entry" ng-if="restaurant.chef.getChefDescription() !== ''">
                                <h4><strong translate="homepage.chef.his_believe"></strong></h4>
                                <p ng-bind="restaurant.chef.getChefDescription()"></p>
                            </div>
                            <div class="slider2_entry" ng-if="restaurant.chef.getChefAward() !== ''">
                                <h4><strong translate="homepage.chef.his_believe"></strong></h4>
                                <p ng-bind="restaurant.chef.getChefAward()"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider4">
                <div class="slide-container" id="kitchen-section" ng-show="pages.homepage.isEnableSection('kitchen')">
                    <h3 translate="homepage.kitchen.meet_the_kitchen"></h3>
                    <div class="container">
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <div class="slide-stage">
                                <div class="slide-image">
                                    <img ng-src="https://website-external.s3.amazonaws.com/media/talaazay/the_kitchen.jpg" alt='the kitchen'/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-9">
                            <div class="slider2_entry">
                                <p  translate="homepage.kitchen.meet_the_kitchen_description"></p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:left"></div>
            <footer>
                <div class="footer">
                    <div class="container">
                        <div class="footer-contact" id="contact-section" ng-show="pages.homepage.isEnableSection('contact')">
                            <!--<h3 translate="homepage.contact.headline"></h3>-->
                            <div class="content-contact">
                                <div class="col-xs-12 col-md-4">
                                    <div class="open-hours">
                                        <h3 translate="homepage.contact.open_hours"></h3>
                                        <div ng-if="restaurant.getRestaurantID() !== 'TH_BK_R_Talayzaa'">
                                            <table>
                                                <tr class="open-hours">
                                                    <td class="open-hours-day">Day</td>
                                                    <td class="open-hours-lunch">Lunch</td>
                                                    <td class="open-hours-dinner">Dinner</td>
                                                </tr>
                                                <tr class="open-hours" ng-repeat="item in restaurant.openhours">
                                                    <td class="open-hours-day" ng-bind="item.getDay()"></td>
                                                    <td class="open-hours-lunch" ng-bind="item.getLunch()"></td>
                                                    <td class="open-hours-dinner" ng-bind="item.getDinner()"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div ng-if="restaurant.getRestaurantID() === 'TH_BK_R_Talayzaa'">
                                            <table>
                                                <p class="text-center contact-address">Open daily  17:30 - 23:30 (Closed for lunch)</p>    
                                            </table>
                                        </div>    
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="contact">
                                        <h3 ng-bind="restaurant.getTitle()"></h3>
                                        <p class="text-center contact-address"><span ng-bind="restaurant.getAddress()"></span><span ng-if="restaurant.getZip() != ''">, {{restaurant.getZip()}}</span><span ng-if="restaurant.getCountry() != ''">, {{restaurant.getCountry()}}</span></p>
                                        <p class="text-center contact-address"><span ng-bind="restaurant.getPhone()"></span></p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div id="restaurant-map" map restaurant="restaurant"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer2"  ng-show="pages.homepage.isEnableSection('newsletter')">
                        <div class="text-6">
                            <h3 translate="homepage.news_letter.title"></h3>
                            <div class="form-email">
                                <form name="NewsLetterForm" ng-submit="NewsLetterForm.$valid && addNewsLetter(restaurant.getRestaurantID(), newsletterEmail)">
                                    <input type="email" ng-model="newsletterEmail" placeholder="{{ 'homepage.news_letter.your_email' | translate }}">
                                    <input type="submit" value="{{ 'homepage.news_letter.btn' | translate }}" id="footer2_button">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="footer3" id="social-section" ng-show="pages.homepage.isEnableSection('social')">
                        <div class="footer3_1 col-xs-12 col-sm-6 col-md-6" id="footer3_1">
                            <!-- <h3 class="widget-title">
                                <i class="instagram-icon"></i>
                            </h3> -->
                            <!-- <ul>
                                <li ng-repeat="image in restaurant.getGallery() | limitTo:3">
                                    <img ng-src="{{ image.getThumbnail() }}" alt="">
                                </li>
                            </ul>      -->
                            <div class="text-center instagram-widget">
                                <div ng-bind-html="instagram_widget | unsafe"></div>
                            </div>
                        </div>
                        <div class="footer3_2 col-xs-12 col-sm-6 col-md-6" id="footer3_2">
                            <div class="fb-page" data-href="{{fb_page_url}}" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="480">
                            </div>
                        </div>
                        <div class="footer4" ng-show="pages.homepage.isEnableSection('relate_page')">
                            <div class="textwidget">
                                <ul>
                                    <li>
                                        <img ng-src="{{asset('images/footer4_img1.png')}}" alt="">
                                    <li>
                                    </li>
                                    <li>
                                        <img ng-src="{{asset('images/footer4_img2.png')}}" alt="">
                                    </li>
                                    <li>
                                        <img ng-src="{{asset('images/footer4_img3.png')}}" alt="">
                                    </li>
                                    <li>
                                        <img ng-src="{{asset('images/footer4_img4.png')}}" alt="">
                                    </li>
                                    <li>
                                        <img ng-src="{{asset('images/footer4_img5.png')}}" alt="">
                                    </li>
                                    <li>
                                        <img ng-src="{{asset('images/footer4_img6.png')}}" alt="">
                                    </li>
                                    <li>
                                        <img ng-src="{{asset('images/footer4_img7.png')}}" alt="">
                                    </li>
                                    <li>
                                        <img ng-src="{{asset('images/footer4_img8.png')}}" alt="">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="footer5">
                            <div class="textwidget">
                                <p translate="homepage.footer" translate-values="{restaurant: restaurant}"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>

</html>
