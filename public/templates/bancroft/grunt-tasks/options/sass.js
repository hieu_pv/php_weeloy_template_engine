module.exports = {
    dist: { // Target
        options: { // Target options
            style: 'expanded'
        },
        files: { // Dictionary of files
            'assets/css/scss/style.css': 'assets/css/scss/style.scss'
        }
    }
}
