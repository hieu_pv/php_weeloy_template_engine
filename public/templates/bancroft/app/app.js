require('jquery');
require('angular');
require('fresco');
require('bootstrap');
require('../../../../angular/tplapp/app');
require('ui.router');
require('./tpl');
require('./shared/direcives/');
require('./components/');

(function(app) {

    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'configProvider', function($stateProvider, $urlRouterProvider, $locationProvider, configProvider) {
        $urlRouterProvider.otherwise(configProvider.Config.base_path);
        $stateProvider
            .state('homepage', {
                url: configProvider.Config.base_path,
                templateUrl: "../app/components/homepage/homepage.tpl.html",
                controller: 'HomeCtrl',
            });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);

    app.run(['$rootScope', '$state', '$translate', 'API', 'Config', function($rootScope, $state, $translate, API, Config) {
        API.restaurant.getFullInfo(Config.restaurant_id)
            .then(function(restaurant) {
                restaurant.setMediaServer($rootScope.media_server);
                $rootScope.restaurant = restaurant;
                $rootScope.setPageTitleUseTranslateService($state.current.name, {
                    restaurant: restaurant
                });
                console.log(restaurant);
            })
            .catch(function(error) {
                throw (error);
            });
            
            
        API.restaurant.getRestaurantWebsiteInfo(Config.restaurant_id)
            .then(function(response) {                
                if(response.data.data.info.description !== ''){
                    $rootScope.restaurant.setDescription(response.data.data.info.description);
                }
                if(response.data.data.info.chef !== ''){
                    $rootScope.restaurant.setChefDescription(response.data.data.info.chef);
                }
            })
            .catch(function(error) {
                throw (error);
            });
            
        $rootScope.$on('$stateChangeSuccess', function(evt, next, current) {
            $("html, body").animate({
                scrollTop: 0
            }, 100);
            $rootScope.setPageTitleUseTranslateService(next.name, {
                restaurant: $rootScope.restaurant
            });
        });
        $rootScope.scrollTop = function(top) {
            $("html, body").animate({
                scrollTop: top
            }, 100);
        };
        try {
            /*
             * Global social data
             */
            API.social.get()
                .then(function(result) {
                    $rootScope.fb_page_url = result.facebook_page;
                    $rootScope.instagram_widget = result.instagram_widget;
                    $rootScope.instagram_url = result.instagram_url;
                })
                .catch(function(error) {
                    throw (error);
                });
            if (typeof FB !== 'undefined') {
                FB.XFBML.parse();
            }
        } catch (e) {
            console.log(e);
        }
    }]);

    app.filter('unsafe', ['$sce', function($sce) {
        return $sce.trustAsHtml;
    }]);

    app.controller('AppCtrl', AppCtrl);
    AppCtrl.$inject = ['$rootScope', '$scope', 'Config', 'API', 'Notification'];

    function AppCtrl($rootScope, $scope, Config, API, Notification) {
        $('.carousel').carousel();
        $scope.SliderControl = function(action) {
            $('.carousel').carousel(action);
        };

        API.restaurant.getRestaurantEvent(Config.restaurant_id)
            .then(function(events) {
                events.forEach(function(value, key) {
                    value.setRestaurant(Config.restaurant_id);
                    events[key] = value;
                });
                console.log(events);
                $scope.events = events;
            })
            .catch(function(error) {
                throw (error);
            });
        $scope.jumpToMenu = function(menu) {
            var id = '#menu-' + menu.ID;
            $scope.jumpTo(id);
        };

        $scope.jumpTo = function(id) {
            var offset = $(id).offset();
            $('#bs-example-navbar-collapse-1').hide();            
            $rootScope.scrollTop(offset.top - 60);
        };
        

        $scope.jumpToBookForm = function() {
            var offset = $('#book-now-div').offset();
            $('#bs-example-navbar-collapse-1').hide();    
            $('#collapse-ss').collapse('show');
            $rootScope.scrollTop(offset.top - 0);
            
        };        
        

        $scope.scrollTop = function() {
            $rootScope.scrollTop(0);
        };

        setTimeout(function() {
            var footerOffset = $('footer').offset();
            $(window).scroll(function() {
                if ($(window).scrollTop() > footerOffset.top) {
                    $rootScope.loadFacebookSDK();
                }
            });
        }, 3000);


        $scope.addNewsLetter = function(email, restaurant) {
            API.common.newsletter(email, restaurant)
                .then(function(result) {
                    Notification.show('success', result.message);
                })
                .catch(function(error) {
                    Notification.show('warning', result.message);
                });
        };

    }
})(angular.module('app', [
    'tplapp',
    'ui.router',
    'app.template',
    'app.directives',
    'app.components',
    'app.components'
]));
