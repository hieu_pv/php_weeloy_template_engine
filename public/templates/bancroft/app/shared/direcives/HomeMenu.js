(function(app) {
    app.directive('homeMenu', theDirective);

    function theDirective() {
        return {
            restrict: 'AE',
            scope: true,
            link: function(scope, element, attrs) {
               $(element).find('.bc-show-list-menu').click(function(){
                    $(element).find('#bs-example-navbar-collapse-1').toggle();
               });
            }
        };
    }
})(angular.module('app.directives.homemenu', []));
