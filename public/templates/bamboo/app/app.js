require('jquery');
require('angular');
require('fresco');
require('bootstrap');
require('../../../../angular/tplapp/app');
require('ui.router');
require('./tpl');
require('./shared/direcives/');
require('./components/');

(function(app) {

    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'configProvider', function($stateProvider, $urlRouterProvider, $locationProvider, configProvider) {
        $urlRouterProvider.otherwise(configProvider.Config.base_path);
        $stateProvider
            .state('homepage', {
                url: configProvider.Config.base_path,
                templateUrl: "../app/components/homepage/homepage.tpl.html",
                controller: 'HomeCtrl',
            })
            .state('menu', {
                url: configProvider.Config.base_path + "menu",
                templateUrl: "../app/components/menu/menu.tpl.html"
            })
            .state('about', {
                url: configProvider.Config.base_path + "about",
                templateUrl: "../app/components/about/about.tpl.html"
            })
            .state('contact', {
                url: configProvider.Config.base_path + "contact",
                templateUrl: "../app/components/contact/contact.tpl.html"
            })
            .state('gallery', {
                url: configProvider.Config.base_path + "gallery",
                templateUrl: "../app/components/gallery/gallery.tpl.html"
            })
            .state('book', {
                url: configProvider.Config.base_path + "book",
                templateUrl: "../app/components/booknow/booknow.tpl.html"
            })
            .state('chef', {
                url: configProvider.Config.base_path + "chef",
                templateUrl: "../app/components/chef/chef.tpl.html"
            })
            .state('social', {
                url: configProvider.Config.base_path + "social",
                templateUrl: "../app/components/social/social.tpl.html",
                controller: "SocialCtrl",
            });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);

    app.run(['$rootScope', '$state', '$translate', 'API', 'Config', function($rootScope, $state, $translate, API, Config) {
        API.restaurant.getFullInfo(Config.restaurant_id)
            .then(function(restaurant) {
                restaurant.setMediaServer($rootScope.media_server);
                $rootScope.restaurant = restaurant;
                $rootScope.setPageTitleUseTranslateService($state.current.name, {
                    restaurant: restaurant
                });
                console.log(restaurant);
            })
            .catch(function(error) {
                throw (error);
            });

            
        API.restaurant.getRestaurantWebsiteInfo(Config.restaurant_id)
            .then(function(response) {                
                if(response.data.data.info.description !== ''){
                    $rootScope.restaurant.setDescription(response.data.data.info.description);
                }
                if(response.data.data.info.chef !== ''){
                    $rootScope.restaurant.setChefDescription(response.data.data.info.chef);
                }
            })
            .catch(function(error) {
                throw (error);
            });

        $rootScope.$on('$stateChangeSuccess', function(evt, next, current) {
            $("html, body").animate({
                scrollTop: 0
            }, 100);
            $rootScope.setPageTitleUseTranslateService(next.name, {
                restaurant: $rootScope.restaurant
            });
        });

        /*
         * Global social data
         */
        API.social.get()
            .then(function(result) {
                $rootScope.fb_page_url = result.facebook_page;
                $rootScope.instagram_widget = result.instagram_widget;
                $rootScope.instagram_url = result.instagram_url;
            })
            .catch(function(error) {
                throw (error);
            });
    }]);

    app.controller('BambooCtrl', BambooCtrl);
    BambooCtrl.$inject = ['$rootScope', '$scope', 'Config'];

    function BambooCtrl($rootScope, $scope, Config) {

    }
})(angular.module('bamboo', [
    'tplapp',
    'ui.router',
    'bamboo.template',
    'bamboo.directives',
    'app.components'
]));
