(function(app) {
    app.controller('HomeCtrl', HomeCtrl);
    HomeCtrl.$inject = ['$rootScope', '$scope', 'API', 'Config'];

    function HomeCtrl($rootScope, $scope, API, Config) {
        $('.carousel').carousel();
        $scope.SliderControl = function(action) {
            $('.carousel').carousel(action);
        };

        API.restaurant.getRestaurantEvent(Config.restaurant_id)
            .then(function(events) {
                console.log(events);
                $scope.events = events;
            })
            .catch(function(error) {
                throw (error);
            });
    }
})(angular.module('app.components.homepage', []));
