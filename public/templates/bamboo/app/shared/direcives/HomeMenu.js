(function(app) {
    app.directive('homeMenu', theDirective);

    function theDirective() {
        return {
            restrict: 'AE',
            scope: true,
            link: function(scope, element, attrs) {
                $(element).find('a.x-btn-navbar.collapsed').click(function() {
                    $(element).find('div.mobile').toggle('3000');
                    // event.stoppropagation();
                });
                $(element).find('div.mobile').find('a').click(function(){
                    console.log('click');
                    $(element).find('div.mobile').toggle('3000');
                });

                // $('a.x-accordion-toggle').click(function(){
                //     var id = $(this).attr('href');
                //     alert(id);
                // });
            }
        };
    }
})(angular.module('app.directives.homemenu', []));
