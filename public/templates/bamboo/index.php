<!DOCTYPE html>
<html lang="" ng-app="bamboo" ng-controller="BambooCtrl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->header(); ?>
        <!-- Bootstrap CSS -->
        <link href="<?php echo $app->getTemplateDirectoryUri(); ?>/assets/css/bamboo.min.css" rel="stylesheet">
        <?php echo $customize->render(); ?>
            <script type="text/javascript" src="<?php echo $app->getTemplateDirectoryUri(); ?>/assets/js/bamboo.js" defer></script>
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <div id='top' class="site">
        <header class="masthead masthead-inline" role="banner">
            <div class="x-navbar-wrap">
                <div class="x-navbar x-navbar-fixed-left">
                    <div class="x-navbar-inner">
                        <div class="x-container max width" home-menu>
                            <h1 class="visually-hidden">Restaurant</h1>
                            <a ng-cloak ui-sref="homepage" class="x-brand img" title="Complete Site For WordPress - X Theme Expanded Demo">
                                <img ng-src="{{ restaurant.getLogoImage() }}" alt="{{ restaurant.getTitle() }}"></a>
                            <a href="#" class="x-btn-navbar collapsed" data-toggle="collapse" data-target=".x-nav-wrap.mobile" name="a-mobile">
                                <i class="fa fa-align-justify" data-x-icon=""></i>
                                <span class="visually-hidden">Navigation</span>
                            </a>
                            <nav class="x-nav-wrap desktop" role="navigation">
                                <ul id="menu-primary-menu" class="x-nav">
                                    <li ng-show="pages.homepage.isEnable()" id="menu-item-276" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-165 current_page_item menu-item-276">
                                        <a ui-sref="homepage"><span translate="left_menu.home"></span></a>
                                    </li>
                                    <li ng-show="pages.about.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-278">
                                        <a ui-sref="about"><span translate="left_menu.about"></span></a>
                                    </li>
                                    <li ng-show="pages.contact.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304">
                                        <a ui-sref="contact"><span translate="left_menu.contact_us"></span></a>
                                    </li>
                                    <li ng-show="pages.menu.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-277">
                                        <a ui-sref="menu"><span translate="left_menu.menu"></span></a>
                                    </li>
                                    <li ng-show="pages.gallery.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-318">
                                        <a ui-sref="gallery"><span translate="left_menu.gallery"></span></a>
                                    </li>
                                    <li ng-show="pages.chef.isEnable()" ng-show="restaurant.chef.hasChefData()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281">
                                        <a ui-sref="chef"><span translate="left_menu.chef"></span></a>
                                    </li>
                                    <li ng-show="pages.book.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281">
<!--                                        <a ui-sref="book"><span ng-bind="restaurant.book_button.text"></span></a>-->
                                            <a ui-sref="book"><span translate="left_menu.book_now"></span></a>
                                    </li>
                                    <li ng-show="pages.social.enable" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281">
                                        <a ui-sref="social"><span translate="left_menu.social"></span></a>
                                    </li>
                                </ul>
                            </nav>
                            <div class="x-nav-wrap mobile collapse">
                                <ul id="menu-primary-menu-1" class="x-nav">
                                    <li ng-show="pages.homepage.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-165 current_page_item menu-item-276">
                                        <a ui-sref="homepage"><span>Home</span></a>
                                    </li>
                                    <li ng-show="pages.about.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-278">
                                        <a ui-sref="about"><span>About</span></a>
                                    </li>
                                    <li ng-show="pages.menu.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-277">
                                        <a ui-sref="menu"><span>Menu</span></a>
                                    </li>
                                    <li ng-show="pages.gallery.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-318">
                                        <a ui-sref="gallery"><span>Gallery</span></a>
                                    </li>
                                    <li ng-show="pages.book.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304">
                                        <a ui-sref="book"><span>Book</span></a>
                                    </li>
                                    <li ng-show="pages.chef.isEnable()" ui-sref-active="active" ng-show="restaurant.chef.hasChefData()" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281">
                                        <a ui-sref="chef"><span>Chef</span></a>
                                    </li>
                                    <li ng-show="pages.social.isEnable()" ui-sref-active="active" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281">
                                        <a ui-sref="social"><span>Social</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div ui-view id="content-agl">
            <?php $this->addPageContent(); ?>
        </div>
        <footer class="x-colophon bottom" role="contentinfo">
            <div class="x-container max width">
                <ul id="menu-primary-menu-2" class="x-nav">
                    <li ng-show="pages.homepage.isEnable()" class="menu-item menu-item-type-post_type menu-item-object-page"><a ui-sref="homepage">Home</a></li>
                    <li ng-show="pages.about.isEnable()" class="menu-item menu-item-type-post_type menu-item-object-page"><a ui-sref="about">About</a></li>
                    <li ng-show="pages.menu.isEnable()" class="menu-item menu-item-type-post_type menu-item-object-page"><a ui-sref="menu">Menu</a></li>
                    <li ng-show="pages.contact.isEnable()" class="menu-item menu-item-type-post_type menu-item-object-page"><a ui-sref="contact">Contact</a></li>
                </ul>
                <div class="x-social-global">
                    <a ng-href="{{fb_page_url}}" class="facebook" title="Facebook" target="_blank"><i class="fa fa-facebook-official" data-x-icon="" aria-hidden="true"></i></a>
                    <a ng-href="{{instagram_url}}" class="instagram" title="Instagram" target="_blank"><i class="fa fa-instagram" data-x-icon="" aria-hidden="true"></i></a>
                    <!-- <a href="#holder" class="google-plus" title="Google+" target="_blank"><i class="fa fa-google-plus-square" data-x-icon="" aria-hidden="true"></i></a> -->
                    <!-- <a href="#holder" class="pinterest" title="Pinterest" target="_blank"><i class="fa fa-pinterest-square" data-x-icon="" aria-hidden="true"></i></a> -->
                </div>
            </div>
        </footer>
    </div>
</body>

</html>
