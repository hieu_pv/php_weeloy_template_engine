module.exports = {

    frontend_template: {
        files: [
            'app/**/*.tpl.html'
        ],
        tasks: [
            'html2js:app',
            'browserify:app'
        ],
        options: {
            spawn: false
        }
    },

    frontend_js: {
        files: ['app/**/*.js', '../../js/*.js'],
        tasks: [
            'jshint:app',
            'browserify',
            'uglify'
        ],
        options: {
            spawn: false
        }
    },


    uglify: {
        files: ['assets/js/main.js'],
        tasks: [
            'uglify:app'
        ],
        options: {
            spawn: false
        }
    },
    frontend_scss: {
        files: ['assets/css/**/*.scss'],
        tasks: [
            'sass:app'
        ]
    },
    cssmin: {
        files: [
            'assets/css/scss/style.css',
        ],
        tasks: [
            'cssmin'
        ]
    },
};
