module.exports = {
    options: {
        //Do not change variables name
        mangle: false
            /**
             * except: ['jQuery', 'angular']
             */
    },
    app: {
        options: {
            sourceMap: false,
        },
        src: ['assets/js/bamboo.js'],
        dest: 'assets/js/bamboo.min.js'
    }
};
