module.exports = {
    options: {
        shorthandCompacting: false,
        roundingPrecision: -1
    },
    target: {
        files: {
            'assets/css/bamboo.min.css': [
                'bower_components/bootstrap/dist/css/bootstrap.min.css',
                'bower_components/font-awesome/css/font-awesome.min.css',
                'assets/css/scss/style.css',
                'assets/css/notification/angular-notify.min.css',
                'assets/css/fresco/fresco.css'
            ]
        }
    }
}
