(function(app) {
    app.controller('SocialCtrl', SocialCtrl);
    SocialCtrl.$inject = ['$rootScope', '$scope', 'Config', 'API'];

    function SocialCtrl($rootScope, $scope, Config, API) {
        $rootScope.loadFacebookSDK();
        try {
            if (typeof FB !== 'undefined') {
                FB.XFBML.parse();
            }
        } catch (e) {
            console.log(e);
        }
    }
    app.filter('unsafe', ['$sce', function($sce) {
        return $sce.trustAsHtml;
    }]);
})(angular.module('app.components.social', []));
