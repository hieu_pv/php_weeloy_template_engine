(function(app) {
    var HOME_WIDGET_TIME = 5000;
    app.controller('HomeCtrl', HomeCtrl);
    HomeCtrl.$inject = ['$rootScope', '$scope'];

    function HomeCtrl($rootScope, $scope) {
        $scope.SliderControl = function(action) {
            $('.carousel').carousel(action);
        };

    }
    app.directive('testimonial', function() {
        return {
            restrict: 'AE',
            scope: true,
            link: function(scope, element, attrs) {
                var first = $(element).find('li').first();
                first.addClass('active');
                scope.$watch(function() {
                    return first.text();
                }, function() {
                    element.css('height', first.height());
                });

                if (!first.is(':last-child')) {
                    setInterval(function() {
                        var currentElement = $(element).find('.active');
                        var next;
                        if (currentElement.is(':last-child')) {
                            next = first;
                        } else {
                            next = currentElement.next();
                        }
                        element.css('height', next.height());
                        currentElement.removeClass('active');
                        next.addClass('active');
                    }, HOME_WIDGET_TIME);
                }
            }
        };
    });
})(angular.module('app.components.homepage', []));
