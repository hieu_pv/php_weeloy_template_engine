angular.module('app.template', ['../app/components/about/about.tpl.html', '../app/components/booknow/booknow.tpl.html', '../app/components/chef/chef.tpl.html', '../app/components/contact/contact.tpl.html', '../app/components/gallery/gallery.tpl.html', '../app/components/homepage/homepage.tpl.html', '../app/components/menu/menu.tpl.html', '../app/components/social/social.tpl.html', '../app/shared/partials/menu.tpl.html']);

angular.module("../app/components/about/about.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/about/about.tpl.html",
    "<div id=\"about\">\n" +
    "    <div id=\"about_section\" style=\"background: url('{{restaurant.getBannerImage()}}') center 0 fixed\">\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <h1 translate=\"about.headline\" translate-values=\"{restaurant: restaurant}\"></h1>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"container about-container\">\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <div class=\"col-xs-12 col-md-3\">\n" +
    "                <div class=\"about-image text-center\">\n" +
    "                    <img ng-src=\"{{ restaurant.about.getChefImage() }}\" alt=\"{{ restaurant.about.getChefName() }}\" class=\"img-responsive\">\n" +
    "                    <h3 ng-bind=\"restaurant.about.getChefName()\"></h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-12 col-md-9\">\n" +
    "                \n" +
    "                <div class=\"his-awards\" ng-repeat=\"description in restaurant.description\">\n" +
    "                    <p class=\"about-info-title\"><strong ng-bind=\"description.title\"></strong></p>\n" +
    "                    <p class=\"about-info-description\" ng-bind=\"description.body\"></p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/booknow/booknow.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/booknow/booknow.tpl.html",
    "<div id=\"book\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <div class=\"text-center\">\n" +
    "                <book-iframe restaurant=\"restaurant\"></book-iframe>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/chef/chef.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/chef/chef.tpl.html",
    "<div id=\"chef\">\n" +
    "    <div id=\"session_thestaurant\" style=\"background: url('{{restaurant.getBannerImage()}}') center 0 fixed\">\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <h1 translate=\"chef.headline\" translate-values=\"{restaurant: restaurant}\"></h1>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"container chef-container\">\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <div class=\"col-xs-12 col-md-3\">\n" +
    "                <div class=\"chef-image text-center\">\n" +
    "                    <img ng-src=\"{{ restaurant.chef.getChefImage()}}\" alt=\"{{ restaurant.chef.getChefName()}}\" class=\"img-responsive\">\n" +
    "                    <h3 ng-bind=\"restaurant.chef.getChefName()\"></h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-12 col-md-9\">\n" +
    "                <div class=\"his-believe\" ng-if=\"restaurant.chef.getChefBelieve() !== ''\">\n" +
    "                    <p class=\"chef-info-title\">\n" +
    "                        <strong ng-if=\"restaurant.chef.isFemale()\" translate=\"chef.her_believe\"></strong>\n" +
    "                        <strong ng-if=\"!restaurant.chef.isFemale()\" translate=\"chef.his_believe\"></strong>\n" +
    "                    </p>\n" +
    "                    <p class=\"chef-info-description\" ng-bind=\"restaurant.chef.getChefBelieve()\"></p>\n" +
    "                </div>\n" +
    "                <div class=\"his-cuisine\" ng-if=\"restaurant.chef.getChefDescription() !== ''\">\n" +
    "                    <p>\n" +
    "                        <strong ng-if=\"restaurant.chef.isFemale()\" translate=\"chef.her_believe\"></strong>\n" +
    "                        <strong ng-if=\"!restaurant.chef.isFemale()\" translate=\"chef.his_believe\"></strong>\n" +
    "                    </p>\n" +
    "                    <p class=\"chef-info-description\" ng-bind=\"restaurant.chef.getChefDescription()\"></p>\n" +
    "                </div>\n" +
    "                <div class=\"his-awards\" ng-if=\"restaurant.chef.getChefAward() !== ''\">\n" +
    "                    <p>\n" +
    "                        <strong ng-if=\"restaurant.chef.isFemale()\" translate=\"chef.her_believe\"></strong>\n" +
    "                        <strong ng-if=\"!restaurant.chef.isFemale()\" translate=\"chef.his_believe\"></strong>\n" +
    "                    </p>\n" +
    "                    <p class=\"chef-info-description\" ng-bind=\"restaurant.chef.getChefAward()\"></p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/contact/contact.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/contact/contact.tpl.html",
    "<div id=\"contact-us\">\n" +
    "    <div id=\"session_contact_Us\" style=\"background: url('{{restaurant.getBannerImage()}}') center 0 fixed\">\n" +
    "        <h1 translate=\"contact.headline\"></h1>\n" +
    "    </div>\n" +
    "    <div class=\"container contact-container\">\n" +
    "        <div class=\"col-xs-12 content_contact_us\">\n" +
    "            <div class=\"col-xs-12 col-sm-8 col-md-8 contact_us_left\">\n" +
    "                <div class=\"form_contact_us\" ng-show=\"pages.contact.isEnableSection('map')\">\n" +
    "                    <div id=\"restaurant-map\" map restaurant=\"restaurant\"></div>\n" +
    "                    <div class=\"description_map\">\n" +
    "                        <h3 translate=\"contact.we_are_waiting_for_you\"></h3>\n" +
    "                        <p translate=\"contact.we_are_waiting_for_you_description\"></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-12 col-sm-4 col-md-4\" ng-show=\"pages.contact.isEnableSection('open_hours')\">\n" +
    "                <div class=\"sidebar\">\n" +
    "                    <h3 ng-bind=\"restaurant.getTitle()\"></h3>\n" +
    "                    <p class=\"text-left\"><span ng-bind=\"restaurant.getAddress()\"></span><span ng-if=\"restaurant.getZip() != ''\">, {{restaurant.getZip()}}</span><span ng-if=\"restaurant.getCountry() != ''\">, {{restaurant.getCountry()}}</span></p>\n" +
    "                    <h3 translate=\"contact.working_hours\" class=\"working-hours-title\"></h3>\n" +
    "                    <div class=\"working-hours\">\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                                <tr class=\"open-hours\">\n" +
    "                                    <td class=\"open-hours-day\"><strong>Day</strong></td>\n" +
    "                                    <td class=\"open-hours-lunch\"><strong>Lunch</strong></td>\n" +
    "                                    <td class=\"open-hours-dinner\"><strong>Dinner</strong></td>\n" +
    "                                </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                                <tr class=\"open-hours\" ng-repeat=\"item in restaurant.openhours\">\n" +
    "                                    <td class=\"open-hours-day\" ng-bind=\"item.getDay()\"></td>\n" +
    "                                    <td class=\"open-hours-lunch\" ng-bind=\"item.getLunch()\"></td>\n" +
    "                                    <td class=\"open-hours-dinner\" ng-bind=\"item.getDinner()\"></td>\n" +
    "                                </tr>\n" +
    "                            </tbody>\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div id=\"contact-form\">\n" +
    "        <div class=\"container\">\n" +
    "            <div class=\"col-xs-12 contact-form-content\">\n" +
    "                <div class=\"col-xs-12 col-sm-8 col-md-8 contact_us_left\">\n" +
    "                    <div class=\"form_contact_us\" ng-show=\"pages.contact.isEnableSection('contact_form')\">\n" +
    "                        <form method=\"post\" name=\"ContactForm\" ng-submit=\"ContactForm.$valid && ContactSubmit(contact)\" novalidate>\n" +
    "                            <div class=\"form-group\">\n" +
    "                                <label for=\"exampleInputEmail1\" class=\"name\">First name</label>\n" +
    "                                <input type=\"text\" class=\"form-control\" name=\"firstname\" ng-model=\"contact.firstname\" ng-pattern=\"/^[a-z ,.'-]+$/i\" required>\n" +
    "                                <div class=\"error\">\n" +
    "                                    <span ng-show=\"ContactForm.$submitted && ContactForm.firstname.$error.required\">Please enter your first name.</span>\n" +
    "                                    <span ng-show=\"ContactForm.$submitted && ContactForm.firstname.$error.pattern\">First name must be alpha only.</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"form-group\">\n" +
    "                                <label for=\"exampleInputPassword1\" class=\"email\">Last name</label>\n" +
    "                                <input type=\"text\" class=\"form-control\" name=\"lastname\" ng-model=\"contact.lastname\" ng-pattern=\"/^[a-z ,.'-]+$/i\" maxlength=\"40\" required>\n" +
    "                                <div class=\"error\">\n" +
    "                                    <span ng-show=\"ContactForm.$submitted && ContactForm.lastname.$error.required\">Please enter your last name.</span>\n" +
    "                                    <span ng-show=\"ContactForm.$submitted && ContactForm.lastname.$error.pattern\">Last name must be alpha only.</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"form-group\">\n" +
    "                                <label for=\"exampleInputPassword1\">Email address</label>\n" +
    "                                <input type=\"email\" class=\"form-control\" name=\"email\" ng-model=\"contact.email\" maxlength=\"40\" ng-pattern=\"/^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$/i\" required>\n" +
    "                                <div class=\"error\">\n" +
    "                                    <span ng-show=\"ContactForm.$submitted && ContactForm.email.$error.required\">Please enter your email.</span>\n" +
    "                                    <span ng-show=\"ContactForm.$submitted && ContactForm.email.$error.pattern\">Please enter a valid email.</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"form-group\">\n" +
    "                                <label>Phone</label>\n" +
    "                                <input class=\"form-control\" type=\"text\" name=\"phone\" ng-model=\"contact.phone\" ng-pattern=\"/^[0-9\\+| ]{6,20}/\" maxlength=\"600\" required>\n" +
    "                                <div class=\"error\">\n" +
    "                                    <span ng-show=\"ContactForm.$submitted && ContactForm.phone.$error.required\">Please enter your phone number.</span>\n" +
    "                                    <span ng-show=\"ContactForm.$submitted && ContactForm.phone.$error.pattern\">Please enter a valid phone number.</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"form-group\">\n" +
    "                                <label for=\"exampleInputPassword1\">Your Message</label>\n" +
    "                                <textarea class=\"form-control\" ng-model=\"contact.message\" name=\"message\" required></textarea>\n" +
    "                                <div class=\"error\">\n" +
    "                                    <span ng-show=\"ContactForm.$sumitted && ContactForm.message.$error.required\">Please enter a message.</span>\n" +
    "                                </div>\n" +
    "                                \n" +
    "                            </div>\n" +
    "                            <input type=\"hidden\" class=\"form-control\" ng-model=\"contact.email_restaurant\" ng-value=\"restaurant.getEmail()\" name=\"email_restaurant\" required/>\n" +
    "                            \n" +
    "                            <button type=\"submit\" class=\"btn\">SEND</button>\n" +
    "                        </form>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-12 col-sm-4 col-md-4\" ng-show=\"pages.contact.isEnableSection('social')\">\n" +
    "                    <div class=\"sidebar\">\n" +
    "                        <h3 translate=\"contact.reservation\"></h3>\n" +
    "                        <button class=\"btn\" id=\"contact-book-btn\">book now</button>\n" +
    "                    </div>\n" +
    "                    <div class=\"sidebar-bottom\">\n" +
    "                        <h3 translate=\"contact.find_us_online\"></h3>\n" +
    "                        <div class=\"list_icon_contact_us\">\n" +
    "                            <a ng-href=\"{{ fb_page_url }}\" class=\"facebook_bg\"><i class=\"fa fa-facebook\"></i></a>\n" +
    "                            <a ng-href=\"{{ instagram_url }}\" class=\"instagram_bg\"><i class=\"fa fa-instagram\"></i></a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/gallery/gallery.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/gallery/gallery.tpl.html",
    "<div id=\"session_foodevent\" style=\"background: url('{{restaurant.getBannerImage()}}') center 0 fixed\">\n" +
    "    <h1 translate=\"gallery.headline\"></h1>\n" +
    "</div>\n" +
    "<div id=\"content\">\n" +
    "    <div class=\"container gallery-container\">\n" +
    "        <div class=\"title_line col-xs-12\">\n" +
    "            <article class=\"gallery\">\n" +
    "                <div ng-repeat=\"image in restaurant.getGallery()\" class=\"gallery-item\">\n" +
    "                    <a ng-href=\"{{ image.getFullsize() }}\" class=\"fresco\" data-fresco-caption=\"{{ restaurant.getTitle() }} - weeloy.com\" data-fresco-group=\"restau\">\n" +
    "                        <div class=\"col-xs-6 col-sm-4 col-md-3\">\n" +
    "                            <div class=\"image\">\n" +
    "                                <img src=\"{{asset('images/gallery.png')}}\" alt=\"{{ restaurant.getTitle() }} - weeloy.com\" style=\"background-image:url('{{ image.getThumbnail() }}'); background-position: 50% 50%; background-size:cover; width:100%; \">\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </article>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/homepage/homepage.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/homepage/homepage.tpl.html",
    "<div id=\"homepage\">\n" +
    "    <div class=\"slider\" ng-show=\"pages.homepage.isEnableSection('slider')\">\n" +
    "        <!-- <img ng-src=\"{{ restaurant.getBannerImage() }}\" alt=\"\" class=\"img-responsive\"> -->\n" +
    "        <div id=\"home-slider\" class=\"carousel slide\" data-ride=\"carousel\">\n" +
    "            <!-- Wrapper for slides -->\n" +
    "            <div class=\"carousel-inner\" role=\"listbox\">\n" +
    "                <div class=\"item\" ng-repeat=\"image in restaurant.getGallery()\" ng-class=\"{'active':$first}\">\n" +
    "                    <img ng-src=\"{{asset('images/slider.png')}}\" alt=\"{{ restaurant.getTitle() }}\" style=\"width: 100%; background-image:url('{{ image.getFullsize() }}'); background-position: 50% 50%; background-size: cover\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <!-- Controls -->\n" +
    "            <a class=\"left carousel-control\" role=\"button\" data-slide=\"prev\" ng-click=\"SliderControl('prev')\">\n" +
    "                <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>\n" +
    "                <span class=\"sr-only\">Previous</span>\n" +
    "            </a>\n" +
    "            <a class=\"right carousel-control\" role=\"button\" data-slide=\"next\" ng-click=\"SliderControl('next')\">\n" +
    "                <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>\n" +
    "                <span class=\"sr-only\">Next</span>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"main\">\n" +
    "    <!-- main -->\n" +
    "    <div class=\"container home-img\">\n" +
    "        <div class=\"flexslider\" ng-if=\"pages.homepage.isEnableSection('testimonial')\">\n" +
    "            <div class=\"home-widget\">\n" +
    "                <ul class=\"slider\" testimonial>\n" +
    "                    <li class=\"\">\n" +
    "                        <p translate=\"homepage.testimonial_1\"></p>\n" +
    "                        <p class=\"testimonial_author\" translate=\"homepage.testimonial_author_1\"></p class=\"testimonial_author\">\n" +
    "                    </li>\n" +
    "                    <li class=\"\">\n" +
    "                        <p translate=\"homepage.testimonial_2\"></p>\n" +
    "                        <p class=\"testimonial_author\" translate=\"homepage.testimonial_author_2\"></p class=\"testimonial_author\">\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"list_menu\" ng-show=\"pages.homepage.isEnableSection('explore_menu')\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <div class=\" col-xs-12 col-sm-4 col-md-4\">\n" +
    "                    <div class=\"list_menu\">\n" +
    "                        <div class=\"list_menu_img\">\n" +
    "                            <a ui-sref=\"menu\">\n" +
    "                                <img ng-src=\"{{ asset('images/homepage_menu.png') }}\" alt=\"\">\n" +
    "                                <div class=\"list_menu_icon\"></div>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                        <div class=\"block-title\">\n" +
    "                            <h3><a ui-sref=\"menu\" translate=\"homepage.explore_our_menu\"></a></h3>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\" col-xs-12 col-sm-4 col-md-4\">\n" +
    "                    <div class=\"list_menu\">\n" +
    "                        <div class=\"list_menu_img\">\n" +
    "                            <a ui-sref=\"about\">\n" +
    "                                <img ng-src=\"{{ asset('images/homepage_menu.png') }}\" alt=\"\">\n" +
    "                                <div class=\"list_menu_icon\"></div>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                        <div class=\"block-title\">\n" +
    "                            <h3><a ui-sref=\"chef\" translate=\"homepage.meet_the_chef\"></a></h3>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\" col-xs-12 col-sm-4 col-md-4\">\n" +
    "                    <div class=\"list_menu\">\n" +
    "                        <div class=\"list_menu_img\">\n" +
    "                            <a ui-sref=\"gallery\">\n" +
    "                                <img ng-src=\"{{ asset('images/homepage_menu.png') }}\" alt=\"\">\n" +
    "                                <div class=\"list_menu_icon\"></div>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                        <div class=\"block-title\">\n" +
    "                            <h3><a ui-sref=\"gallery\" translate=\"homepage.gallery\"></a></h3>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- end Main -->\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/menu/menu.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/menu/menu.tpl.html",
    "<div id=\"menu-page\">\n" +
    "    <div id=\"session\" style=\"background: url('{{restaurant.getBannerImage()}}') center 0 fixed\">\n" +
    "        <h1 translate=\"menu_page.headline\"></h1>\n" +
    "    </div>\n" +
    "    <div id=\"content\">\n" +
    "        <div class=\"container\">\n" +
    "            <div class=\"title_line\" ng-repeat=\"menu in restaurant.getMenus()\">\n" +
    "                <h1>\n" +
    "                    <img ng-src=\"{{asset('images/menu/title_line_left.png')}}\" alt=\"\">\n" +
    "                    <span ng-bind=\"menu.categorie.value\"></span>\n" +
    "                    <img ng-src=\"{{asset('images/menu/title_line_right.png')}}\" alt=\"\">\n" +
    "                </h1>\n" +
    "                <article ng-repeat=\"item in menu.items\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-xs-12 col-sm-3 col-md-3 img-menu\" >\n" +
    "                            <div class=\"verlay\" ng-show=\"item.mimage\">\n" +
    "                                <a href=\"#\" ><img ng-src=\"{{restaurant.getMenuImage(item.mimage)}}\" alt=\"\"></a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-xs-12 col-sm-9 col-md-9\">\n" +
    "                            <div class=\"nine_columns\">\n" +
    "                                <h1>{{ item.item_title }} <span class=\"pull-right\">{{item.currency}}{{item.price}}</span></h1>\n" +
    "                                <p ng-bind=\"item.item_description\"></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </article>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/social/social.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/social/social.tpl.html",
    "<div id=\"social\">\n" +
    "    <div class=\"container social-container\">\n" +
    "        <div class=\"col-xs-12 col-md-6\">\n" +
    "            <h3><a ng-href=\"{{fb_page_url}}\" translate=\"social.find_us_on_facebook\"></a></h3>\n" +
    "            <div class=\"fb-page\" data-href=\"{{fb_page_url}}\" data-tabs=\"timeline\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12 col-md-6\">\n" +
    "            <h3><a ng-href=\"{{ instagram_url }}\" translate=\"social.find_us_on_instagram\"></a></h3>\n" +
    "            <div ng-bind-html=\"instagram_widget | unsafe\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/shared/partials/menu.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/shared/partials/menu.tpl.html",
    "menu");
}]);
