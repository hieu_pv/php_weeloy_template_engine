<!DOCTYPE html>
<html lang="en" ng-app="igloo" ng-controller="IglooCtrl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->header(); ?>
        <link rel="stylesheet" href="<?php echo $app->getTemplateDirectoryUri(); ?>/assets/css/theme.min.css">
        <?php echo $this->customize->render(); ?>
            <script src="<?php echo $app->getTemplateDirectoryUri() ?>/assets/js/igloo.js" defer></script>
</head>

<body>
    <div id="page">
        <header id="header">
            <div class="header-reponsive">
                <div class="icon-toggle">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </div>
                <div>
                    <h1><em ng-bind="restaurant.getTitle()"></em></h1>
                </div>
            </div>
            <div class="container">
                <div id="tophead">
                    <div class="row">
                        <div class="six_columns social-icons">
                            <a ng-href="{{ fb_page_url }}"><i class="fa fa-facebook"></i></a>
                            <a ng-href="{{ instagram_url }}"><i class="fa fa-instagram"></i></a>
                        </div>
                        <div class="six_columns six_columns2">
                            <a ui-sref="book" translate="header.message"></a>
                        </div>
                    </div>
                </div>
                <div class="mainhead">
                    <div class="logo">
                        <a ui-sref="homepage">
                            <img ng-src="{{ restaurant.getLogoImage() }}" alt="">
                        </a>
                    </div>
                    <div class="menu hid pull-right">
                        <!-- menu -->
                        <nav class="navbar">
                            <div class="container-fluid">
                                <div class="navbar-collapse">
                                    <ul class="nav navbar-nav navbar-collapse">
                                        <li ui-sref-active="active" ng-show="pages.homepage.isEnable()">
                                            <a ui-sref="homepage" translate="menu.homepage">Home</a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="about" ng-show="pages.about.isEnable()" translate="menu.about">About</a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="contact" ng-show="pages.contact.isEnable()" translate="menu.contact">Contact</a>
                                        </li>
                                        <li ui-sref-active="active" ng-show="pages.menu.isEnable()">
                                            <a ui-sref="menu" translate="menu.menu">Menu</a>
                                        </li>
                                        <li ui-sref-active="active" ng-show="pages.gallery.isEnable()">
                                            <a ui-sref="gallery" translate="menu.gallery">Gallery</a>
                                        </li>
                                        <li ui-sref-active="active" ng-show="pages.book.isEnable()">
                                            <a ui-sref="book" translate="menu.booking">Booking</a>
                                        </li>
                                        <li ui-sref-active="active" ng-if="restaurant.chef.hasChefData()" ng-show="pages.chef.isEnable()">
                                            <a ui-sref="chef" translate="menu.chef">Chef</a>
                                        </li>
                                        <li ui-sref-active="active" ng-show="pages.social.isEnable()">
                                            <a ui-sref="social" translate="menu.social">Social</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <!-- menu -->
                </div>
            </div>
        </header>
        <!-- /header -->
        <div ui-view></div>
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-top">
                        <div class="col-xs-12 col-md-4">
                            <div class="four columns">
                                <div class="footer-logo">
                                    <h3 ng-bind="restaurant.getTitle()"></h3>
                                    <div class="footer-logo-image col-xs-4 col-md-4">
                                        <img style="max-width:100%;" ng-src="{{ restaurant.getLogoImage() }}" alt=""></p>
                                    </div>
                                    <div  class="col-xs-8 col-md-8">
                                        <span translate="footer.restaurant_infomation"></span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="four columns">
                                <h3 translate="footer.gallery_widget"></h3>
                                <div class="fgroup">
                                    <ul>
                                        <li ng-repeat="image in restaurant.getGallery() | limitTo:4">
                                            <a ng-href="{{image.getFullsize()}}" class="fresco" data-fresco-group="footer">
                                                <img ng-src="{{ asset('images/footer.png') }}" alt="restaurant.getTitle()" style="width: 100%;background-image:url('{{ image.getThumbnail(media_server) }}');background-size:cover;background-position:50% 50%;">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="four columns">
                                <h3 translate="footer.text_widget_title"></h3>
                                <p translate="footer.text_widget"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>
