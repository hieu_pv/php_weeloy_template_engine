module.exports = {
    dist: { // Target
        options: { // Target options
            style: 'expanded'
        },
        files: { // Dictionary of files
            'assets/css/scss/igloo.css': 'assets/css/scss/igloo.scss'
        }
    }
}
