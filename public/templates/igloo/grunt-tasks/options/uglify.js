module.exports = {
    options: {
        //Do not change variables name
        mangle: false
        /**
         * except: ['jQuery', 'angular']
         */
    },
    app: {
        options: {
            sourceMap: false,
        },
        src: ['assets/js/igloo.js'],
        dest: 'assets/js/igloo.min.js'
    }
};