<?php
namespace App\libs;

use App\libs\File;
use Exception;

class Language extends File
{
    private $folderDir;
    public function __construct($template_name, $language_folder, $language)
    {
        $this->folderDir = __DIR__ . '/../../public/templates/' . $template_name . '/assets/languages/' . $language_folder;
        $this->setDir($this->folderDir . '/' . $language . '.json');
    }

    public function getAllLanguageFile()
    {
        $scandir = scandir($this->folderDir);
        $files   = [];
        foreach ($scandir as $key => $value) {
            if (isset($value) && $value != '.' && $value != '..' && !is_dir($value)) {
                array_push($files, $value);
            }
        }
        return $files;
    }

    public function insert($data)
    {
        $newfile = $this->folderDir . '/' . $data->name . '.json';
        if (file_exists($newfile)) {
            throw new Exception("{$data->name} already exist", 1);
        } else {
            $file = fopen($newfile, "w");
            if (!$file) {
                throw new Exception("Unable to create {$data->name}", 1);
            } else {
                fwrite($file, json_encode($data->data));
                fclose($file);
                return true;
            }
        }
    }
}
