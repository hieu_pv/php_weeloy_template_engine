<?php

namespace App\libs;

use App\libs\File;

class Page extends File
{
    private $base_path;
    public function __construct($template_name, $foldername, $base_path)
    {
        $this->base_path = $base_path;
        $this->setDir(__DIR__ . '/../../public/templates/' . $template_name . '/assets/customize/' . $foldername . '/pages.json');
    }
    /**
     * detect current page
     * @return string $page_name
     */
    public function getCurrentPage()
    {
        $uri  = $_SERVER['REQUEST_URI'];
        $uri  = preg_replace('/\?(.*)/', '', $uri);
        $path = explode($this->base_path, $uri);
        $path = (isset($path[1]) && $path[1] != '') ? $path[1] : '';
        if ($path != '/') {
            $path = '/' . $path;
        }
        $pages     = $this->read();
        $validPage = false;
        $page_name = '';
        foreach ($pages as $key => $page) {
            if ($page->path == $path) {
                $validPage = true;
                $page_name = $key;
                break;
            }
        }
        if ($validPage) {
            return $page_name;
        } else {
            return null;
        }
    }
}
