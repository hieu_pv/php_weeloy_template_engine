<?php

namespace App\libs;

use Exception;
use GuzzleHttp\Client;

class Auth
{
    private $client;
    private $restaurant_id;
    private $user;
    public function __construct($api_url, $restaurant_id)
    {
        $this->restaurant_id = $restaurant_id;
        $this->client        = new Client(['base_uri' => $api_url]);
        if (isset($_SESSION['auth'])) {
            $this->_setUser($_SESSION['auth']);
        }
    }
    public function login($email, $password)
    {
        $uri      = '/api/template/login/' . $this->restaurant_id;
        $response = $this->client->request('POST', $uri, [
            'json'    => [
                'email'    => $email,
                'password' => $password,
            ],
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);
        $body = $response->getBody();
        $data = json_decode($body->getContents());
        if ($data->status == 0) {
            throw new Exception($data->errors, 0);
        } else {
            $this->_setUser($data->data);
            return $data->data;
        }
    }

    public function logout()
    {
        $this->user = null;
        unset($_SESSION['auth']);
    }

    public function isLoggedIn()
    {
        return isset($this->user);
    }

    /**
     * Gets the value of user.
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the value of user.
     *
     * @param mixed $user the user
     *
     * @return self
     */
    private function _setUser($user)
    {
        $_SESSION['auth'] = $user;
        $this->user       = $user;

        return $this;
    }
}
