<?php
namespace App;

use App\libs\App;
use App\libs\Auth;
use App\libs\Customize;
use App\libs\File;
use App\libs\Language;
use App\libs\Page;
use App\libs\Transform;
use App\models\Other;

class Controller
{
    public $app;
    private $other;
    private $customize;
    private $language;
    private $transform;
    private $auth;
    public function __construct()
    {
        @set_exception_handler([$this, 'exceptionHandler']);
        $this->app       = new App();
        $this->other     = new File(__DIR__ . '/../public/templates/' . $this->app->getTemplateName() . '/assets/customize/' . $this->app->getCustomFolder() . '/social.json');
        $this->customize = new Customize($this->app->getTemplateName(), $this->app->getCustomFolder(), $this->app->getCustomFile());
        $this->page      = new Page($this->app->getTemplateName(), $this->app->getCustomFolder(), $this->app->getBasePath());
        $this->language  = new Language($this->app->getTemplateName(), $this->app->getLanguageFolder(), $this->app->getDefaultLanguage());
        $this->auth      = new Auth($this->app->getBaseApiURL(), $this->app->getRestaurantID());
        $this->transform = new Transform();
    }
    /**
     * Return error object and header 'HTTP/1.0 500 Error' when an error occur
     */
    public function exceptionHandler($exception)
    {
        return $this->error($exception);
    }

    /**
     * Add config data
     * @return string config object for javascript
     */
    public function addConfigData()
    {
        $base_url = $this->app->getBaseURL();
        $config   = [
            'base_url'         => $base_url,
            'base_path'        => $this->app->getBasePath(),
            'base_api_url'     => $this->app->getBaseApiURL(),
            'base_book_url'    => $this->app->getBaseBookURL(),
            'template_name'    => $this->app->getTemplateName(),
            'default_language' => $this->app->getDefaultLanguage(),
            'default_booking_form' => $this->app->getDefaultLanguageBookingForm(),
            'language_folder'  => $this->app->getLanguageFolder(),
            'restaurant_id'    => $this->app->getRestaurantID(),
            'facebook_app_id'  => $this->app->getFacebookAppID(),
        ];
        $output = '<script type="text/javascript">var config = ' . json_encode($config) . ';</script>';
        return $output;
    }
    /*
     * Add restaurantfullinfo
     */
    public function addRestaurantFullInfo()
    {
        $output = '<script type="text/javascript">var restaurant_full_info_api_data = ' . json_encode($this->app->getRestaurantApiData()) . ';</script>';
        return $output;
    }
    /**
     * Create meta tag
     * @return String meta data
     */
    public function addMetaTagAndTitle()
    {
        $page   = $this->page->getCurrentPage();
        $data   = $this->language->read();
        $output = '';
        // Generate page title

        $output .= '<title>' . $this->transform->convert($this->app, $data->page_title->$page) . '</title>';
        // Generate meta tag
        foreach ($data->meta->$page as $metatag) {
            $tag = '<meta ';
            foreach ($metatag->attributes as $meta_key => $meta_value) {
                $meta_value = $this->transform->convert($this->app, $meta_value);
                $tag .= $meta_key . '="' . $meta_value . '" ';
            }
            $tag .= '/>';
            $output .= $tag;
        }
        return $output;
    }

    /*
     * Add Page content, include file from php_page_content folder
     */
    public function addPageContent()
    {
        $dir = __DIR__ . '/../public/templates/' . $this->app->getTemplateName() . '/php_page_content/' . $this->page->getCurrentPage() . '.php';
        include $dir;
    }

    /**
     * @return String html for header
     */
    public function header()
    {
        return $this->addMetaTagAndTitle() . $this->addRestaurantFullInfo() . $this->addConfigData();
    }

    /**
     * Return public/templates/{template_name}/index.php for template app
     */
    public function getIndex()
    {
        $app       = $this->app;
        $customize = $this->customize;
        require __DIR__ . '/../public/templates/' . $app->getTemplateName() . '/index.php';
    }
    /**
     * Return public/templates/{template_name}/admin/index.php for template admin app
     */
    public function getAdmin()
    {
        $app = $this->app;
        require __DIR__ . '/../public/admin.php';
    }
    /**
     * API get customize.json
     * @return Object customize.json
     */
    public function getCustomize()
    {
        return $this->response($this->customize->read());
    }
    /**
     * Edit customie.json
     * @param int $index, Object $data
     */
    public function putCustomize()
    {
        $input     = $this->request();
        $customize = $this->customize->read();
        if (!isset($customize[$input->index])) {
            throw new Exception("item not found", 101);
        } else {
            $customize[$input->index]->variables = $input->item->variables;
            $data                                = $this->customize->save($customize);
            return $this->response($data);
        }
    }
    /**
     * Read input data
     */
    public function request()
    {
        $rawData = file_get_contents("php://input");
        return json_decode($rawData);
    }
    /**
     * Return header 'HTTP/1.0 200 OK' && 'Content-Type: application/json' for success api
     * @param $data
     */
    public function response($data = null)
    {
        header('HTTP/1.0 200 OK');
        header('Content-Type: application/json');
        if (isset($data)) {
            echo json_encode(['data' => $data]);
        } else {
            echo json_encode(['data' => true]);
        }
    }
    /**
     * Return header 'HTTP/1.0 500 Error' && 'Content-Type: application/json'
     */
    public function error($error = null)
    {
        header('HTTP/1.0 500 Error');
        header('Content-Type: application/json');
        if (isset($error)) {
            echo json_encode(['error' => $error->getMessage(), 'error_code' => $error->getCode()]);
        } else {
            echo 'error';
        }
    }
    /*
     * Return customize style
     */
    public function getCustomizeStyle()
    {
        return $this->response($this->customize->render());
    }
    /**
     * Upload Image
     * @return String $url
     */
    public function postUploadImage()
    {
        try {
            $filename = $_FILES['file']['name'];
            $folder   = __DIR__ . '/../public/images/customize/';
            if (!is_dir($folder)) {
                mkdir($folder);
            }
            $destination = $folder . $filename;
            if (file_exists($destination)) {
                $i = 1;
                while (file_exists($destination)) {
                    $i++;
                    $filename    = $i . '-' . $filename;
                    $destination = $folder . $filename;
                }
            }
            $data = move_uploaded_file($_FILES['file']['tmp_name'], $destination);
            $url  = $this->app->getBaseURL() . '/images/customize/' . $filename;
            return $data ? $this->response($url) : $this->response($data);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 1);
        }
    }
    /**
     * @return data in language file
     */
    public function getLanguage()
    {
        return $this->response($this->language->read());
    }
    /**
     * Update language file
     * @return mixed
     */
    public function putLanguage()
    {
        $data = $this->request();
        return $this->response($this->language->save($data->data));
    }
    /**
     * Create new language
     * @return mixed
     */
    public function postLanguage()
    {
        $data = $this->request();
        return $this->response($this->language->insert($data));
    }
    /**
     * @return all file in language folder
     */
    public function getAllLanguageFile()
    {
        return $this->response($this->language->getAllLanguageFile());
    }
    /**
     * get all pages from pages.json
     *
     * @return mixed
     */
    public function getPages()
    {
        return $this->response($this->page->read());
    }

    /**
     * Change Page status visible/hiden
     *
     * @return mixed
     */
    public function putPages()
    {
        $data                     = $this->request();
        $pagename                 = $data->pagename;
        $pages                    = $this->page->read();
        $pages->$pagename->enable = !$pages->$pagename->enable;
        $this->page->save($pages);
        return $this->response($pages->$pagename);
    }

    /**
     * change section enable status true/false
     *
     * @return mixed
     */
    public function putPageSection($vars)
    {
        $pagename   = $vars['page'];
        $data       = $this->request();
        $section_id = $data->section_id;
        $pages      = $this->page->read();
        $page       = $pages->$pagename;
        foreach ($page->sections as $key => $value) {
            if ($value->id == $section_id) {
                $page->sections[$key]->enable = !$page->sections[$key]->enable;
                $pages->$pagename             = $page;
                $section                      = $page->sections[$key];
            }
        }
        $this->page->save($pages);
        if (isset($section)) {
            return $this->response($section);
        } else {
            throw new Exception("can not find section {$section_id}", 1009);
        }
    }

    /**
     * get other data ex: social data
     *
     * @return mixed
     */
    public function getSocial()
    {
        return $this->response($this->other->read());
    }

    /**
     * edit other data
     *
     * @return boolean
     */
    public function putSocial()
    {
        $data = $this->request();
        $data = new Other($data);
        return $this->response($this->other->save($data));
    }

    /**
     * login user to application
     *
     * @param string $email, string password
     * @return boolean
     */
    public function postLogin()
    {
        $data = $this->request();
        $user = $this->auth->login($data->email, $data->password);
        return $this->response($user);
    }

    /**
     * logout current user
     *
     * @return boolean
     */
    public function postLogout()
    {
        $this->auth->logout();
        return $this->response();
    }

}
return new Controller();
