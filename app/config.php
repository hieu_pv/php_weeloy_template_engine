<?php
    return [
    /*
     |--------------------------------------------------------------------------
     | Application Domains
     |--------------------------------------------------------------------------
     |
     | Array of all domain with restaurant ID and template name
     | @params string domain => domain of application
     |   string base_path => base path of application
     |   string template_name => the name of template will be display
     |   string restaurant_id => the id of restaurant will be display
     |   string default_language => the name of language will be display as default
     |   string facebook_app_id => facebook app id
     |   string customize_filename => name of json file to store all customize of that domain, each template has a folder /assets/customize/{customize_foldername}/{customize_filename}.json
     |   string customize_foldername => name of folder to store all customize of that domain, each template has a folder /assets/customize/{customize_foldername}
     |   string language_folder => name of folder to store all text of that domain with many language, each template has a folder /assets/languages/{language_folder}/{language_name}.json
     */
    'domains'       => [
        'dev-template1:8888'   => [
            'base_path'            => '/',
            'template_name'        => 'bamboo',
            'restaurant_id'        => 'SG_SG_R_TheFunKitchen',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'bamboo_dev',
            'customize_foldername' => 'bamboo_dev',
            'language_folder'      => 'bamboo_dev',
        ],
        'dev-template22:8888'   => [
            'base_path'            => '/',
            'template_name'        => 'igloo',
            'restaurant_id'        => 'SG_SG_R_DruryLane',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'igloo_dev',
            'customize_foldername' => 'igloo_dev',
            'language_folder'      => 'igloo_dev',
        ],
        'dev-template33:8888'   => [
            'base_path'            => '/',
            'template_name'        => 'bancroft',
            'restaurant_id'        => 'SG_SG_R_TheFunKitchen',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'bancroft_dev',
            'customize_foldername' => 'bancroft_dev',
            'language_folder'      => 'bancroft_dev',
        ],
        'thefunkitchensg.weeloy.com'   => [
            'base_path'            => '/',
            'template_name'        => 'bamboo',
            'restaurant_id'        => 'SG_SG_R_TheFunKitchen',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'fun_kitchen',
            'customize_foldername' => 'fun_kitchen',
            'language_folder'      => 'fun_kitchen',
        ],
        'thefunkitchensg2.weeloy.com'    => [
            'base_path'            => '/',
            'template_name'        => 'igloo',
            'restaurant_id'        => 'SG_SG_R_TheFunKitchen',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'customize',
            'customize_foldername' => 'igloo_dev',
            'language_folder'      => 'igloo_dev',
        ],
        'bancroft.dev' => [
            'base_path'            => '/',
            'template_name'        => 'bancroft',
            'restaurant_id'        => 'SG_SG_R_TheFunKitchen',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'bancroft_dev',
            'customize_foldername' => 'bancroft_dev',
            'language_folder'      => 'bancroft_dev',
        ],
        'drurylane.weeloy.com'    => [
            'base_path'            => '/',
            'template_name'        => 'igloo',
            'restaurant_id'        => 'SG_SG_R_DruryLane',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'drury_lane',
            'customize_foldername' => 'drury_lane',
            'language_folder'      => 'drury_lane',
        ],
        'dev-template2:8888'    => [
            'base_path'            => '/',
            'template_name'        => 'igloo',
            'restaurant_id'        => 'SG_SG_R_DruryLane',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'drury_lane',
            'customize_foldername' => 'drury_lane',
            'language_folder'      => 'drury_lane',
        ],
        'thekoreantable.weeloy.com' => [
            'base_path'            => '/',
            'template_name'        => 'bamboo',
            'restaurant_id'        => 'KR_SE_R_TheKoreanTable',
            'default_language'     => 'kr',
            'facebook_app_id'      => '',
            'customize_filename'   => 'table_korean',
            'customize_foldername' => 'table_korean',
            'language_folder'      => 'table_korean',
        ],
         'dev-template4:8888' => [
            'base_path'            => '/',
            'template_name'        => 'bamboo',
            'restaurant_id'        => 'KR_SE_R_TheKoreanTable',
            'default_language'     => 'kr',
            'facebook_app_id'      => '',
            'customize_filename'   => 'table_korean',
            'customize_foldername' => 'table_korean',
            'language_folder'      => 'table_korean',
        ],
        'eight.weeloy.com' => [
            'base_path'            => '/',
            'template_name'        => 'bancroft',
            'restaurant_id'        => 'SG_SG_R_EightCafeBar',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'eight',
            'customize_foldername' => 'eight',
            'language_folder'      => 'eight',
        ],
        'dev-template3:8888' => [
            'base_path'            => '/',
            'template_name'        => 'bancroft',
            'restaurant_id'        => 'SG_SG_R_EightCafeBar',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'eight',
            'customize_foldername' => 'eight',
            'language_folder'      => 'eight',
        ],
        
        'latabledelydia:8888'   => [
            'base_path'            => '/',
            'template_name'        => 'bamboo',
            'restaurant_id'        => 'FR_PR_R_LaTableDeLydia',
            'default_language'     => 'fr',
            'default_booking_form'     => 'fr',
            'facebook_app_id'      => '',
            'customize_filename'   => 'table_lydia',
            'customize_foldername' => 'table_lydia',
            'language_folder'      => 'table_lydia',
        ],
        'latabledelydia.weeloy.com'   => [
            'base_path'            => '/',
            'template_name'        => 'bamboo',
            'restaurant_id'        => 'FR_PR_R_LaTableDeLydia',
            'default_language'     => 'fr',
            'default_booking_form'     => 'zz',
            'facebook_app_id'      => '',
            'customize_filename'   => 'table_lydia',
            'customize_foldername' => 'table_lydia',
            'language_folder'      => 'table_lydia',
        ],
        'talayzaa:8888'   => [
            'base_path'            => '/',
            'template_name'        => 'bancroft',
            'restaurant_id'        => 'TH_BK_R_Talayzaa',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'talay_zaa',
            'customize_foldername' => 'talay_zaa',
            'language_folder'      => 'talay_zaa',
        ],
        'talayzaa.weeloy.com'   => [
            'base_path'            => '/',
            'template_name'        => 'bancroft',
            'restaurant_id'        => 'TH_BK_R_Talayzaa',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'talay_zaa',
            'customize_foldername' => 'talay_zaa',
            'language_folder'      => 'talay_zaa',
        ]
    ],
    /*
     |--------------------------------------------------------------------------
     | Base API URL
     |--------------------------------------------------------------------------
     |
     | the URL to retrieve all data
     |
     */
    'base_api_url'  => 'http://www.weeloy.com/api',
    /*
     |--------------------------------------------------------------------------
     | Base Book URL
     |--------------------------------------------------------------------------
     |
     | the URL for booking form
     |
     */
    'base_book_url' => 'https://www.weeloy.com',
    ];
    
    ?>

