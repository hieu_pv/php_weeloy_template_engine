module.exports = function(grunt) {
    // The 'build' task gets your app ready to run for development and testing.
    grunt.registerTask('build', [
        'jshint',
        'html2js',
        'browserify',
        'sass',
        'cssmin',
        'uglify'
    ]);
};
