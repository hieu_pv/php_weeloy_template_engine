module.exports = {
    options: {
        //Do not change variables name
        mangle: false
            /**
             * except: ['jQuery', 'angular']
             */
    },
    app: {
        options: {
            sourceMap: false,
        },
        src: ['public/js/tplapp.js'],
        dest: 'public/js/tplapp.min.js'
    },
    admin: {
        options: {
            sourceMap: false,
        },
        src: ['public/js/tpladmin.js'],
        dest: 'public/js/tpladmin.min.js'
    }
};
