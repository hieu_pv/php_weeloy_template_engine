module.exports = {
    app: {
        options: {
            module: 'tplapp.template',
        },
        src: [
            'angular/tplapp/**/*.tpl.html'
        ],
        dest: 'angular/tplapp/tpl.js'
    },
    admin: {
        options: {
            module: 'tpladmin.template',
        },
        src: [
            'angular/tpladmin/**/*.tpl.html'
        ],
        dest: 'angular/tpladmin/tpl.js'
    }
};
