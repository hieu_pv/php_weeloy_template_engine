(function(app) {
    app.controller('LanguageCreateCtrl', LanguageCreateCtrl);
    LanguageCreateCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification'];

    function LanguageCreateCtrl($rootScope, $scope, API, Notification) {
        $scope.submit = function(name, data) {
            API.language.insert({
                    name: name,
                    data: data
                })
                .then(function(response) {
                    Notification.show('success', 'Create New Language successful');
                    var filename = name + '.json';
                    $rootScope.LanguageFiles.push(filename);
                })
                .catch(function(error) {
                    Notification.show('warning', error.data.error);
                    throw (error);
                });
        };
    }
    app.filter('json', function() {
        return function(object) {
            return JSON.stringify(object);
        };
    });
})(angular.module('admin.components.language.create', []));
