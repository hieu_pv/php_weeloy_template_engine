(function(app) {
    app.controller('LanguageEditCtrl', LanguageEditCtrl);
    app.filter('typeof', typeofFilter);
    LanguageEditCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', 'Notification'];

    function LanguageEditCtrl($rootScope, $scope, $stateParams, API, Notification) {
        $rootScope.reloadLanguageData();
        $('.collapse').collapse('show');
        $scope.save = function() {
            API.language.update({
                    name: $stateParams.filename,
                    data: $rootScope.language,
                })
                .then(function(response) {
                    Notification.show('success', 'Save Successful');
                })
                .catch(function(error) {
                    Notification.show('warning', error.data.error);
                    throw (error);
                });
        };
        $scope.cancel = function() {
            $rootScope.reloadLanguageData();
            Notification.show('success', 'Reloaded');
        };
    }

    function typeofFilter() {
        return function(object) {
            return typeof object;
        };
    }
})(angular.module('admin.components.language.edit', []));
