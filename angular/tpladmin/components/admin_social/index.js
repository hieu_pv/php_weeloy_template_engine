(function(app) {
    app.controller('SocialCtrl', SocialCtrl);
    SocialCtrl.$inject = ['$rootScope', '$scope', 'Config', 'API', 'Notification'];

    function SocialCtrl($rootScope, $scope, Config, API, Notification) {
        $scope.update = function(data) {
            API.social.update(data)
                .then(function(response) {
                    Notification.show('success', 'Update successful');
                })
                .catch(function(error) {
                    Notification.show('warning', error.data.error);
                    throw (error);
                });
        };
        API.social.get()
            .then(function(result) {
                $scope.social = result;
            })
            .catch(function(error) {
                throw (error);
            });
    }
})(angular.module('admin.components.social', []));
