(function(app) {
    app.controller('LoginCtrl', LoginCtrl);
    LoginCtrl.$inject = ['$rootScope', '$scope', '$state', 'API', 'Notification'];

    function LoginCtrl($rootScope, $scope, $state, API, Notification) {
        if ($rootScope.isLoggedIn) {
            $state.go('homepage');
        }
        $scope.login = function(user) {
            API.user.login(user)
                .then(function(result) {
                    Notification.show('success', 'Logged in success');
                    $rootScope.isLoggedIn = true;
                    $rootScope.user = result;
                    $state.go('homepage');
                })
                .catch(function(error) {
                    console.log(error);
                    Notification.show('warning', error);
                    throw (error);
                });
        };
    }
})(angular.module('admin.components.login', []));
