require('./tpl');
(function(app) {
    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'configProvider', function($stateProvider, $urlRouterProvider, $locationProvider, configProvider) {
        var admin_base_path = configProvider.Config.base_path;
        if (admin_base_path == '/') {
            admin_base_path += 'admin/';
        } else {
            admin_base_path += '/admin/';
        }
        $urlRouterProvider.otherwise(admin_base_path);
        $stateProvider
            .state('login', {
                url: admin_base_path + 'login',
                templateUrl: "../angular/tpladmin/components/login/login.tpl.html",
                controller: 'LoginCtrl'
            })
            .state('profile', {
                url: admin_base_path + 'update-password',
                templateUrl: "../angular/tpladmin/components/profile/profile.tpl.html",
                controller: 'ProfileCtrl'
            })
            .state('homepage', {
                url: admin_base_path,
                templateUrl: "../angular/tpladmin/components/admin_homepage/admin_homepage.tpl.html",
            })
            .state('customize', {
                url: admin_base_path + 'customize/:page',
                templateUrl: "../angular/tpladmin/components/admin_customize/admin_customize.tpl.html",
                controller: 'CustomizeCtrl',
            })
            .state('language', {
                url: admin_base_path + 'language',
                templateUrl: "../angular/tpladmin/components/admin_language/admin_language.tpl.html",
            })
            .state('language.edit', {
                url: '/edit/:filename',
                templateUrl: "../angular/tpladmin/components/admin_language/edit/edit.tpl.html",
                controller: "LanguageEditCtrl",
            })
            .state('language.create', {
                url: '/create',
                templateUrl: "../angular/tpladmin/components/admin_language/create/create.tpl.html",
                controller: "LanguageCreateCtrl"
            })
            .state('social', {
                url: admin_base_path + 'social',
                templateUrl: "../angular/tpladmin/components/admin_social/admin_social.tpl.html",
                controller: "SocialCtrl"
            });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);
})(angular.module('admin.routes', ['ui.router', 'tpladmin.template']));
