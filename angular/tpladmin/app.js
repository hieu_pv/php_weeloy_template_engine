require('jquery');
require('angular');
require('bootstrap');
require('ui.router');
require('../tplapp/app');
require('./routes');
require('./components/');
require('./directives/');

(function(app) {
    app.run(['$window', '$rootScope', '$urlRouter', '$state', 'API', 'Notification', function($window, $rootScope, $urlRouter, $state, API, Notification) {

        $rootScope.isLoggedIn = $window.isLoggedIn;
        $rootScope.user = $window.user;
        /*
         * Global variable $root.LanguageFiles
         */
        API.language.files()
            .then(function(result) {
                $rootScope.LanguageFiles = result;
            })
            .catch(function(error) {
                throw (error);
            });
        /*
         * Global variable language
         */
        function loadLanguageData() {
            API.language.get()
                .then(function(result) {
                    $rootScope.language = result;
                })
                .catch(function(error) {
                    throw (error);
                });
        }
        loadLanguageData();
        $rootScope.reloadLanguageData = function() {
            loadLanguageData();
        };
        /*
         * GET all pages
         */
        API.customize.pages()
            .then(function(result) {
                $rootScope.pages = result;
            })
            .catch(function(error) {
                throw (error);
            });
        $rootScope.$on('$locationChangeSuccess', function(evt) {
            $rootScope.$state = $state;
            if (!$rootScope.isLoggedIn) {
                $state.go('login');
            }
        });

        $rootScope.logout = function() {
            API.user.logout()
                .then(function(result) {
                    Notification.show('success', 'You are logged out');
                    document.location.reload();
                })
                .catch(function(error) {
                    Notification.show('warning', 'Error');
                    throw (error);
                });
        };
    }]);
})(angular.module('admin', [
    'tplapp',
    'admin.routes',
    'admin.components',
    'admin.directives',
]));
