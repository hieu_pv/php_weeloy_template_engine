(function(app) {
    var tplappConfig = {
        base_url: window.config.base_url || '//weeloy.codersvn.com',
        base_path: window.config.base_path || '/',
        base_api_url: window.config.base_api_url || '//weeloy.codersvn.com/api',
        base_book_url: window.config.base_book_url || '//weeloy.codersvn.com',
        default_language: window.config.default_language || 'en',
        default_booking_form: window.config.default_booking_form || 'en',
        language_folder: window.config.language_folder,
        restaurant_id: window.config.restaurant_id || '',
        template_name: window.config.template_name,
        facebook_app_id: window.config.facebook_app_id || '1647781885480803',
        media_server: ['//media1.weeloy.com', '//media2.weeloy.com', '//media3.weeloy.com', '//media4.weeloy.com', '//media5.weeloy.com'],
    };

    app.provider('config', function() {
        return {
            Config: tplappConfig,
            $get: function() {
                return config;
            }
        };
    });
    app.factory('Config', theFactory);
    theFactory.$inject = ['$window'];

    function theFactory($window) {
        return tplappConfig;
    }

})(angular.module('tplapp.config', []));
