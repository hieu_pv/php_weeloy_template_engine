var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function RestaurantReview(options) {
    this.ambiance_scrore = '';
    this.food_score = '';
    this.price_score = '';
    this.service_score = '';
    this.score = '';
    this.booking_date = '';
    this.comment = '';
    this.user_name = '';
    this.user_picture = '';
    this.post_date = '';

    BaseModel.call(this, options);
}

inherits(RestaurantReview, BaseModel);

RestaurantReview.prototype.getScore = function() {
    return this.score;
};

RestaurantReview.prototype.getAmbianceScore = function() {
    return this.ambiance_scrore;
};

RestaurantReview.prototype.getPriceScore = function() {
    return this.price_score;
};

RestaurantReview.prototype.getFoodScore = function() {
	return this.food_score;
};

RestaurantReview.prototype.getServiceScore = function() {
    return this.service_score;
};

RestaurantReview.prototype.getUsername = function() {
	return this.user_name;
};

module.exports = RestaurantReview;