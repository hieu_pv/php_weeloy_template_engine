var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function RestaurantImage(options) {
    this.filename = '';
    this.mediaServer = '//media.weeloy.com';
    this.thumbnailSize = '/270/';
    this.fullsize = '/1440/';
    this.restaurant = '';
    BaseModel.call(this, options);
}

inherits(RestaurantImage, BaseModel);

RestaurantImage.prototype.getFilename = function() {
    return this.filename;
};

RestaurantImage.prototype.getThumbnail = function(mediaServer) {
    mediaServer = mediaServer || this.mediaServer;
    return mediaServer + '/upload/restaurant/' + this.restaurant + this.thumbnailSize + this.filename;
};

RestaurantImage.prototype.getFullsize = function(mediaServer) {
    mediaServer = mediaServer || this.mediaServer;
    return mediaServer + '/upload/restaurant/' + this.restaurant + this.fullsize + this.filename;
};

module.exports = RestaurantImage;
