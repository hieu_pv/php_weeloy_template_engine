var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function PageMenu(options) {
    this.path = '';
    this.enable = true;
    this.sections = [];
    BaseModel.call(this, options);
}
inherits(PageMenu, BaseModel);

PageMenu.prototype.getPath = function() {
    return this.path;
};

PageMenu.prototype.isEnable = function() {
    return this.enable;
};

PageMenu.prototype.getSections = function() {
    return this.sections;
};

PageMenu.prototype.isEnableSection = function(section_id) {
    var sections = this.getSections();
    var enable = true;
    sections.forEach(function(value, key) {
        if (value.id === section_id) {
            if (value.enable !== undefined && typeof value.enable === 'boolean') {
                enable = value.enable;
            }
        }
    });
    return enable;
};

module.exports = PageMenu;
