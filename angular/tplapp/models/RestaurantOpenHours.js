var BaseModel = require('./BaseModel');
var inherits = require('inherits');
var moment = require('moment');

function RestaurantOpenHours(options) {
    this.day = '';
    this.dinner = '';
    this.lunch = '';

    BaseModel.call(this, options);
}

inherits(RestaurantOpenHours, BaseModel);

RestaurantOpenHours.prototype.getDay = function() {
    return this.day;
};

RestaurantOpenHours.prototype.getDinner = function() {
    return this.dinner;
};

RestaurantOpenHours.prototype.getLunch = function() {
    return this.lunch;
};

module.exports = RestaurantOpenHours;
