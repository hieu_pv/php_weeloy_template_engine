var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function RestaurantChef(options) {
    this.mediaServer = '//media.weeloy.com';
    this.restaurant = '';
    this.chef_award = '';
    this.chef_believe = '';
    this.chef_description = '';
    this.chef_image = '';
    this.chef_name = '';
    this.chef_origin = '';
    this.chef_type = '';
    BaseModel.call(this, options);
}

inherits(RestaurantChef, BaseModel);

RestaurantChef.prototype.getChefAward = function() {
    return this.chef_award;
};
RestaurantChef.prototype.getChefBelieve = function() {
    return this.chef_believe;
};
RestaurantChef.prototype.getChefDescription = function() {
    return this.chef_description;
};
RestaurantChef.prototype.getChefName = function() {
    return this.chef_name;
};
RestaurantChef.prototype.getChefOrigin = function() {
    return this.chef_origin;
};
RestaurantChef.prototype.getChefImage = function(mediaServer) {
    mediaServer = mediaServer || this.mediaServer;
    return mediaServer + '/upload/restaurant/' + this.restaurant + '/' + this.chef_image;
};
RestaurantChef.prototype.hasChefData = function() {
    return this.getChefName() === '' ? false : true;
};
RestaurantChef.prototype.isFemale = function() {
    if (this.chef_type.match(/female/gi) === null) {
        return false;
    } else {
        return true;
    }
};
module.exports = RestaurantChef;
