var BaseModel = require('./BaseModel');
var inherits = require('inherits');
var moment = require('moment');

function RestaurantEvent(options) {
    this.ID = '';
    this.city = '';
    this.country = '';
    this.description = '';
    this.start = '';
    this.end = '';
    this.morder = '';
    this.name = '';
    this.picture = '';
    this.title = '';
    this.restaurant = '';
    this.mediaServer = '//media.weeloy.com';

    BaseModel.call(this, options);
}

inherits(RestaurantEvent, BaseModel);

RestaurantEvent.prototype.getID = function() {
    return this.ID;
};

RestaurantEvent.prototype.getCity = function() {
    return this.city;
};

RestaurantEvent.prototype.getCountry = function() {
    return this.country;
};

RestaurantEvent.prototype.getDescription = function() {
    return this.description;
};

RestaurantEvent.prototype.getStartTime = function(format) {
    return moment(this.start).format(format);
};

RestaurantEvent.prototype.getEndTime = function(format) {
    return moment(this.end).format(format);
};

RestaurantEvent.prototype.getOrder = function() {
    return this.morder;
};

RestaurantEvent.prototype.getName = function() {
    return this.name;
};

RestaurantEvent.prototype.getPicture = function(mediaServer) {
    mediaServer = mediaServer || this.mediaServer;
    return mediaServer + '/upload/restaurant/' + this.restaurant + '/' + this.picture;
};

RestaurantEvent.prototype.getTitle = function() {
    return this.title;
};

RestaurantEvent.prototype.setRestaurant = function(restaurant_id) {
    this.restaurant = restaurant_id;
    return this;
};

module.exports = RestaurantEvent;
