require('angular-resource');
require('./config');
require('./exception');
require('./api/');
require('./tpl');
require('./language/');
require('./directives/');
require('./services/');
require('ng-file-upload');
require('ng-file-upload-shim');
var Customize = require('./models/Customize');

(function(app) {
    app.run(['$rootScope',
        '$location',
        '$window',
        '$translate',
        'Config',
        'Notification',
        'API',
        function($rootScope, $location, $window, $translate, Config, Notification, API) {
            /*
             * Global asset function - return url of assets folder
             */
            $rootScope.asset = function(file) {
                return Config.base_url + '/templates/' + Config.template_name + '/assets/' + file;
            };
            /*
             * Global variable for media server
             */
            $rootScope.media_server = Config.media_server[Math.floor(Math.random() * Config.media_server.length)];
            /*
             * Global function for book button
             */
            $rootScope.BookNow = function(restaurant) {
                var url = Config.base_book_url + '/' + restaurant.getInternalPath() + "/book-now";
                window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
            };
            /*
             * Global function to set page title
             */
            $rootScope.setPageTitle = function(title) {
                document.title = title;
            };
            /*
             * Global function to set page title use translate service
             */
            $rootScope.setPageTitleUseTranslateService = function(route_name, translate_values) {
                $translate('page_title.' + route_name, translate_values).then(function(data) {
                    document.title = data;
                });
            };

            /*
             * global pages variable
             */
            API.customize.pages()
                .then(function(pages) {
                    $rootScope.pages = pages;
                    console.log(pages);
                })
                .catch(function(error) {
                    throw (error);
                });

            /*
             * Global function to load Facebook SDK
             */
            $rootScope.loadFacebookSDK = function() {
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                window.fbAsyncInit = function() {
                    FB.init({
                        appId: Config.facebook_app_id,
                        cookie: true, // enable cookies to allow the server to access 
                        xfbml: true, // parse social plugins on this page
                        version: 'v2.5' // use version 2.5
                    });
                };
            };
            /*
             * Global function for contact form
             */
            $rootScope.ContactSubmit = function(contact) {
                API.common.sendContact(contact.firstname, contact.lastname, contact.email, contact.email_restaurant, contact.message)
                    .then(function(response) {
                        console.log(response);
                        Notification.show('success', response);
                    })
                    .catch(function(error) {
                        Notification.show('warning', warning);
                    });
            };
        }
    ]);

})(angular.module('tplapp', [
    'tplapp.language',
    'tplapp.config',
    'tplapp.exception',
    'tplapp.api',
    'tplapp.directives',
    'tplapp.services',
    'tplapp.template',
    'ngFileUpload',
    'ngResource'
]));
