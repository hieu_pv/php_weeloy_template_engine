var PageMenu = require('../../models/PageMenu');
var Customize = require('../../models/Customize');
(function(app) {
    app.service('CustomizeService', theService);
    theService.$inject = ['$rootScope', '$resource', '$http', '$q', '$window', 'ApiUrl', 'Upload'];

    function theService($rootScope, $resource, $http, $q, $window, ApiUrl, Upload) {
        var url = ApiUrl.getApiInCurrentDomain('/customize');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            update: {
                method: 'PUT'
            }
        });

        this.get = function() {
            var defferred = $q.defer();
            resource.get(function(response) {
                if (response.data === null) {
                    defferred.reject('customize.json not found or invalid');
                } else {
                    var customize = [];
                    response.data.forEach(function(value) {
                        customize.push(new Customize(value));
                    });
                    defferred.resolve(customize);
                }
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };

        this.update = function(data) {
            var defferred = $q.defer();
            data = data || {};
            resource.update({}, data, function(result) {
                if (result.data === false) {
                    defferred.reject('customize.json is not writeable');
                } else {
                    defferred.resolve(result.data);
                }
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        };

        this.upload = function(file) {
            var defferred = $q.defer();
            if (file === undefined || file === null) {
                defferred.reject('File not found');
            }
            var data = {
                file: file,
            };
            var url = ApiUrl.getApiInCurrentDomain('/customize/upload');
            Upload.upload({
                url: url,
                data: data
            }).then(function(response) {
                if (response.data.data === false) {
                    defferred.reject('Can not upload file');
                } else {
                    defferred.resolve(response.data.data);
                }
            }, function(error) {
                defferred.reject(error);
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $rootScope.$broadcast('UploadProgressPercent', progressPercentage);
            });
            return defferred.promise;
        };

        this.pages = function() {
            var defferred = $q.defer();
            var API_URL = ApiUrl.getApiInCurrentDomain('/pages');
            $http.get(API_URL, {
                cache: true,
            }).then(function(response) {
                var pages = response.data.data;
                for (var key in pages) {
                    pages[key] = new PageMenu(pages[key]);
                }
                defferred.resolve(pages);
            }, function(error) {
                defferred.reject('can not get pages');
            });
            return defferred.promise;
        };

        this.changePageSectionStatus = function(page, section) {
            var defferred = $q.defer();
            var API_URL = ApiUrl.getApiInCurrentDomain('/page/' + page + '/section');
            var data = {
                section_id: section.id
            };
            $http.put(API_URL, data).then(function(response) {
                var section = response.data.data;
                defferred.resolve(section);
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };

        this.changePageStatus = function(pagename) {
            var defferred = $q.defer();
            var API_URL = ApiUrl.getApiInCurrentDomain('/pages');
            var data = {
                pagename: pagename
            };
            $http.put(API_URL, data).then(function(response) {
                var page = new PageMenu(response.data.data);
                defferred.resolve(page);
            }, function(error) {
                defferred.reject('can not get pages');
            });
            return defferred.promise;
        };

    }

})(angular.module('tplapp.api.customize', []));
