(function(app) {
    app.service('CommonService', theService);
    theService.$inject = ['$http', '$q', 'ApiUrl'];

    function theService($http, $q, ApiUrl) {
        this.sendContact = function(firstname, lastname, email, email_restaurant, message) {
            alert(email_restaurant);
            var API_URL = ApiUrl.get('/send-website-contact-email');
            var data = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                email_restaurant: email_restaurant,
                message: message,
            };
            var defferred = $q.defer();

            var config = {
                method: 'POST',
                url: API_URL,
                headers: {
                    'Content-Type': undefined
                },
                data: data
            };
            $http(config)
                .then(function(response) {
                    if (response.data.status === 1) {
                        defferred.resolve(response.data.data);
                    } else {
                        defferred.reject('can not send request');
                    }
                }, function(error) {
                    defferred.reject('can not send request');
                });
            return defferred.promise;
        };

        this.newsletter = function(restaurant, email) {
            var API_URL = ApiUrl.get('/newsletter/externalRestaurant/addEmail/' + restaurant + '/' + email);
            var defferred = $q.defer();
            $http.get(API_URL)
                .then(function(response) {
                    if (response.data.status == 1) {
                        defferred.resolve(response.data.data);
                    } else {
                        defferred.reject(response.errors);
                    }
                }, function(error) {
                    defferred.reject('add newsletter error');
                });
            return defferred.promise;
        };
    }
})(angular.module('tplapp.api.common', []));
