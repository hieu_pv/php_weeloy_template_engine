(function(app) {
    app.service('UserService', UserService);
    UserService.$inject = ['$http', '$q', 'ApiUrl'];

    function UserService($http, $q, ApiUrl) {
        this.login = function(params) {
            params = params || {};
            var defferred = $q.defer();
            var url = ApiUrl.getApiInCurrentDomain('/login');
            $http.post(url, params)
                .then(function(response) {
                    defferred.resolve(response.data.data);
                }, function(error) {
                    defferred.reject(error.data.error);
                });
            return defferred.promise;
        };
        this.logout = function() {
            var defferred = $q.defer();
            var url = ApiUrl.getApiInCurrentDomain('/logout');
            $http.delete(url, {})
                .then(function(response) {
                    defferred.resolve(response.data.data);
                }, function(error) {
                    defferred.reject(error);
                });
            return defferred.promise;
        };
        this.update = function(user_id, old_password, new_password) {
            var defferred = $q.defer();
            var data = {
                old_password: old_password,
                new_password: new_password
            };
            var url = ApiUrl.get('/template/users/update/' + user_id);
            var config = {
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': undefined
                },
                data: data
            };
            $http(config)
                .then(function(response) {
                    if (response.data.status == 1) {
                        defferred.resolve(response.data.data);
                    } else {
                        defferred.reject(response.data.errors);
                    }
                }, function(error) {
                    defferred.reject(error);
                });
            return defferred.promise;
        };
    }
})(angular.module('app.api.user', []));
