var Restaurant = require('../../models/Restaurant');
var RestaurantReview = require('../../models/RestaurantReview');
var RestaurantEvent = require('../../models/RestaurantEvent');
var RestaurantService = require('../../models/RestaurantService');
var RestaurantOpenHours = require('../../models/RestaurantOpenHours');
var RestaurantImage = require('../../models/RestaurantImage');
var RestaurantChef = require('../../models/RestaurantChef');

(function(app) {
    app.service('RestaurantService', theService);
    theService.$inject = ['$http', '$q', '$window', 'ApiUrl'];

    function theService($http, $q, $window, ApiUrl) {

        // Return all infomation of a restaurant
        this.getFullInfo = function(query) {
            var defferred = $q.defer();
            
            if ($window.restaurant_full_info_api_data !== undefined && $window.restaurant_full_info_api_data !== null) {
                defferred.resolve(setRestaurantData($window.restaurant_full_info_api_data.data));
            } else {
                var API_URL = ApiUrl.get('/restaurantfullinfo/' + query);
                $http.get(API_URL, {
                    cache: true,
                }).then(function(response) {
                    if (response.data.status === 1) {
                        defferred.resolve(setRestaurantData(response.data.data));
                    } else {
                        defferred.reject('error');
                    }
                }, function(error) {
                    defferred.reject(error);
                });
            }
            return defferred.promise;
        };

        function setRestaurantData(data) {
            var restaurant = new Restaurant(data.restaurantinfo);
            var openhours = [];
            var gallery = [];
            restaurant.openhours.forEach(function(value) {
                openhours.push(new RestaurantOpenHours(value));
            });
            restaurant.images.forEach(function(value, key) {
                gallery.push(new RestaurantImage({
                    restaurant: restaurant.getRestaurantID(),
                    filename: value,
                }));
            });
            restaurant.gallery = gallery;
            restaurant.openhours = openhours;
            restaurant.setMenu(data.menu);
            var chef = data.chef;
            chef.restaurant = restaurant.getRestaurantID();
            restaurant.chef = new RestaurantChef(chef);
            return restaurant;
        }

        // Return list of reviews of a restaurant
        this.getReviews = function(RestaurantID, page) {

            if (page === undefined) {
                page = 1;
            }
            var API_URL = ApiUrl.get('/getreviews/' + RestaurantID + '/list/' + page);
            var defferred = $q.defer();

            $http.get(API_URL, {
                cache: true
            }).then(function(response) {
                if (response.data.status !== 1 || response.data.count < 1) {
                    defferred.reject('error');
                } else {
                    var reviews = [];
                    response.data.data.reviews.forEach(function(value) {
                        reviews.push(new RestaurantReview(value));
                    });
                    defferred.resolve(reviews);
                }
            }, function() {
                defferred.reject('Can not get data from server');
            });
            return defferred.promise;
        };

        // Return list of events of a restaurant
        this.getRestaurantEvent = function(RestaurantID) {
            var API_URL = ApiUrl.get('/restaurant/events/active/' + RestaurantID);
            var defferred = $q.defer();
            $http.get(API_URL).then(function(response) {
                if (response.data.status !== 1) {
                    defferred.reject('error');
                } else {
                    if (response.data.data.count < 1) {
                        throw ('error null');
                    } else {
                        var events = [];
                        response.data.data.event.forEach(function(value) {
                            events.push(new RestaurantEvent(value));
                        });
                        defferred.resolve(events);
                    }
                }
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };


        this.getCateringPictures = function(RestaurantID, limit) {
            var API_URL = ApiUrl.get('/restaurant/catering/gallery/' + RestaurantID + '/' + limit);
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: true
            }).then(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };

        // Return All of services of a restaurant
        this.getRestaurantService = function(RestaurantID) {
            var API_URL = ApiUrl.get('/restaurant/service/restaurantservices/' + RestaurantID + '/active_only');
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: true,
            }).success(function(response) {
                if (response.status === 1) {
                    var services = [];
                    response.data.restaurantservices.forEach(function(value, key) {
                        services.push(new RestaurantService(value));
                    });
                    defferred.resolve(services);
                } else {
                    defferred.reject('error');
                }
            });
            return defferred.promise;
        };
        
         this.getRestaurantWebsiteInfo = function(RestaurantID) {
            var API_URL = ApiUrl.get('/v2/external_website/info/' + RestaurantID);
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: true
            }).then(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        
    }

})(angular.module('tplapp.api.restaurant', []));
