(function(app) {
    app.factory('SocialService', factory);
    factory.$inject = [
        'ApiUrl',
        '$resource',
        '$q'
    ];

    function factory(
        ApiUrl,
        $resource,
        $q
    ) {
        var service = {
            get: get,
            update: update,
        };

        var url = ApiUrl.getApiInCurrentDomain('/social');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            update: {
                method: 'PUT'
            }
        });

        function get() {
            var defferred = $q.defer();
            resource.get({}, function(response) {
                defferred.resolve(response.data.data.social);
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        }

        function update(data) {
            var defferred = $q.defer();
            resource.update({
                social: data
            }, function(response) {
                defferred.resolve(response.data);
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        }

        return service;
    }

})(angular.module('tplapp.api.social', []));
