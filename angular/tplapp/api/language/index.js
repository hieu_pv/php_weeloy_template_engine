(function(app) {
    app.service('LanguageService', theService);
    theService.$inject = ['$http', '$resource', '$q', '$window', 'ApiUrl'];

    function theService($http, $resource, $q, $window, ApiUrl) {
        var url = ApiUrl.getApiInCurrentDomain('/language');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            insert: {
                method: 'POST'
            },
            update: {
                method: 'PUT'
            }
        });

        this.get = function() {
            var defferred = $q.defer();
            resource.get(function(response) {
                if (response.data === null) {
                    defferred.reject('language file not found or invalid');
                } else {
                    var language = response.data;
                    // language = addSelector(language);
                    defferred.resolve(language);
                }
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };

        function addSelector(object) {
            if (object.selector === undefined) {
                object.selector = '';
            }
            for (var key in object) {
                if (typeof object[key] === 'object') {
                    if (object.selector === '') {
                        object[key].selector = key;
                    } else {
                        object[key].selector = object.selector + '.' + key;
                    }
                    addSelector(object[key]);
                }
            }
            return object;
        }

        this.insert = function(data) {
            var defferred = $q.defer();
            data = data || {};
            resource.insert({}, data, function(result) {
                defferred.resolve(result.data);
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        };

        this.update = function(data) {
            var defferred = $q.defer();
            data = data || {};
            resource.update({}, data, function(result) {
                defferred.resolve(result.data);
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        };

        this.files = function() {
            var defferred = $q.defer();
            $http.get(ApiUrl.getApiInCurrentDomain('/language/files'))
                .then(function(response) {
                    defferred.resolve(response.data.data);
                }, function(error) {
                    defferred.reject(error);
                });
            return defferred.promise;
        };

    }

})(angular.module('tplapp.api.language', []));
