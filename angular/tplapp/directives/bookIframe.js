(function(app) {
    app.directive('bookIframe', ['API', '$window', '$sce', 'Config', function(API, $window, $sce, Config) {
        return {
            restrict: 'AE',
            scope: {
                restaurant: '='
            },
            template: '<iframe width="100%" style="border: none; background: transparent" height="800" ng-src="{{url}}"></iframe>',
            link: function(scope, element, attrs) {
                var background;
                var panel_background;
                API.customize.get()
                    .then(function(result) {
                        result.forEach(function(value, key) {
                            if (value.getSelector() === 'bookIframe') {
                                value.variables.forEach(function(v, k) {
                                    if (v.name == '$background') {
                                        background = v.data;
                                    }
                                    if (v.name == '$panel_background') {
                                        panel_background = v.data;
                                    }
                                });
                            }
                            var url = Config.base_book_url + '/' + scope.restaurant.getInternalPath() + "/book-now";

                            if (background !== undefined && background !== '') {
                                url += '/' + encodeURIComponent(background);
                            }
                            if (panel_background !== undefined && panel_background !== '') {
                                url += '/' + encodeURIComponent(panel_background);
                            }
                            
                            url+= '?bktracking=WEBSITE';
                            
                            if(Config.default_booking_form){
                                url = url + '&langue=' + Config.default_booking_form;
                            }
                            
                            scope.url = $sce.trustAsResourceUrl(url);
                            
                        });
                    })
                    .catch(function(error) {
                        throw (error);
                    });
            },
        };
    }]);

})(angular.module('tplapp.directives.bookIframe', []));
