(function(app) {
    app.directive('map', ['$window', function($window) {
        return {
            restrict: 'AE',
            scope: {
                restaurant: '='
            },
            link: function(scope, element, attrs) {
                scope.$watch('restaurant', function(restaurant) {
                    if (restaurant !== undefined) {
                        if (document.getElementById('gmap-sdk') !== undefined && document.getElementById('gmap-sdk') !== null) {
                            CreateMap(scope.restaurant.getLatitude().lat, scope.restaurant.getLatitude().lng);
                        } else {
                            var script = document.createElement("script");
                            script.type = "text/javascript";
                            script.id = 'gmap-sdk';
                            script.src = "//maps.google.com/maps/api/js?key=AIzaSyCygRzzUBq5PyqG6ctOROO5i1Pc0yr52I8&callback=CreateMap";
                            document.body.appendChild(script);
                        }

                        $window.CreateMap = function() {
                            CreateMap(scope.restaurant.getLatitude().lat, scope.restaurant.getLatitude().lng);
                        };
                    }

                    function CreateMap(lat, lng) {
                        var myLatlng = new google.maps.LatLng(lat, lng);
                        var mapOptions = {
                            zoom: 14,
                            center: myLatlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            scrollwheel: false,
                            navigationControl: false,
                            mapTypeControl: false,
                        };
                        if ($(window).width < 767) {
                            mapOptions.scaleControl = false;
                            mapOptions.draggable = false;
                            mapOptions.zoomControl = false;
                        }
                        map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                        });
                    }
                });
            },
        };
    }]);

})(angular.module('tplapp.directives.map', []));
